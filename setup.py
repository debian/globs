## setup.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##


from distutils.core import setup

setup(name='globs',
	version='0.2',
	description='GL Open Benchmark Suite',
	author='Angelo "Encelo" Theodorou',
	author_email='encelo@users.sourceforge.net',
	url='http://globs.sourceforge.net',
	license = 'GNU GENERAL PUBLIC LICENSE',
	classifier = 'Topic :: System :: Benchmark',
	package_dir={'': 'src'},
	packages=['Globs'],
	scripts=['src/globs'],
	data_files=[('share/globs/ui', ['ui/about_dlg.ui', 'ui/hwd_win.ui', 'ui/local_db_win.ui', 'ui/main_win.ui', 'ui/pref_win.ui']),
				('share/globs/pixmaps', ['pixmaps/globs.png']),
				('share/applications', ['globs.desktop'])]
	)
