#!/usr/bin/env python
## patchdir.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import os
import sys

# Checking shell arguments
if len(sys.argv) > 2:
	print 'Usage: ' + sys.argv[0] + ' prefix=PREFIX_DIR'
	sys.exit(-5)
elif len(sys.argv) == 1:
	prefix = '/'
else:
	arg1 = sys.argv[1]
	splitted = arg1.split('=')
	if splitted[0] == 'prefix':
		prefix = splitted[1]
	else:
		print 'Usage: ' + sys.argv[0] + ' prefix=PREFIX_DIR'
		sys.exit(-5)

# Program running script
# Reading original file
fname = 'src/globs'
try:
	f = open(fname, 'r')
except IOError:
	print 'File ' + fname + " doesn't exist!"
lines = f.readlines()
f.close()

# Patching
lines_p = []
for line in lines:
	if line.find('share_dir =') != -1:
		line = 'share_dir = ' + "'" + os.path.join(prefix, 'share/globs') + "'" + '\n'
	elif line.find('locale_dir =') != -1:
		line = 'locale_dir = ' + "'" + os.path.join(prefix, 'share/locale') + "'" + '\n'
	elif line.find('bench_dir =') != -1:
		line = 'bench_dir = ' + "'" + os.path.join(prefix, 'share/globs') + "'" + '\n'
	lines_p.append(line)

# Writing patched file
#f = open(fname, 'w')
f = open(fname, 'w')
f.writelines(lines_p)
f.close()


# .desktop file
# Reading original file
fname = 'globs.desktop'
try:
	f = open(fname, 'r')
except IOError:
	print 'File ' + fname + "doesn't exist!"
	exit(-1)
lines = f.readlines()
f.close()

# Patching
lines_p = []
for line in lines:
	if line.find('Icon=') != -1:
		line = 'Icon=' + os.path.join(prefix, 'share/globs/pixmaps', 'globs.png') + '\n'
	lines_p.append(line)

# Writing patched file
#f = open(fname, 'w')
f = open(fname, 'w')
f.writelines(lines_p)
f.close()
