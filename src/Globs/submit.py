## submit.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import urllib2
import urllib
import Globs

class Submit:
	"""Interface between the application and the common results remote database"""
	def __init__(self, db_cls, user_cls, hwd_cls):
		self.db = db_cls
		self.user = user_cls
		self.hwd = hwd_cls

		# Defining constants for analyzing parse() return list
		# Benchmark neither inserted nor updated
		self.PARSE_ERROR_CONN = -1     # Error of the connection
		self.PARSE_ERROR_URL = -2      # Malformed URL
		self.PARSE_ERROR_DB_CONN = -3  # Error connecting to the database
		self.PARSE_ERROR_DB_SEL = -4   # Error selecting the database
		self.PARSE_ERROR_PASSWORD = -5 # Wrong password
		self.PARSE_NOTHING = -6        # Result didn't meet update conditions
		# -------------------
		# Benchmark either inserted or updated
		self.PARSE_UPDATED = 1         # Updated
		self.PARSE_INSERTED = 2        # Inserted
		self.PARSE_INSERTED_NM = 3     # Inserted on a new machine
		self.PARSE_INSERTED_NUNM = 4   # Inserted on a new machine by a new user
		# -------------------


	def send(self, date):
		"""Create an encoded URL with parameters and append it to the script URL"""
		params = {}
		
		# User
		params['user_id'] = self.user['user']
		params['user_password'] = self.user['password']
		params['user_name'] = self.user['name']
		params['user_location'] = self.user['location']
		params['user_email'] = self.user['email']
		params['user_homesite'] = self.user['homesite']

		# Hardware
		cpu = self.hwd['cpu']
		mem = self.hwd['mem']
		gl = self.hwd['gl']

		params['hostname'] = self.hwd['host']
		params['cpu_model'] = cpu['model']
		params['cpu_frequency'] = cpu['frequency']
		params['cpu_bogomips'] = cpu['bogomips']
		params['mem_total'] = mem['physical']
		params['gl_renderer'] = gl['renderer']
		params['gl_version'] = gl['version']
		
		# Benchmark
		fetchall = self.db.select("WHERE date='%s'" % date)
		row = fetchall[0]

		params['bench_name'] = row[0]
		params['bench_version'] = row[1]
		params['bench_fps'] = row[2]
		params['bench_width'] = row[3]
		params['bench_height'] = row[4]
		params['bench_fullscreen'] = row[5]
		params['bench_time'] = row[6]

		# Check parameters integrity
		if params['user_id'] == None or \
			params['user_password'] == None or \
			params['hostname'] == None or \
			params['bench_name'] == None or \
			params['bench_fps'] == None:
			print(_('One or more compelling parameter is None!'))
			return False
		
		enc_params = urllib.urlencode(params)
		# Debug
		#print('\n', enc_params)
		# -----
		
		try:
			file = urllib2.urlopen(Globs.SUBMIT_URL, enc_params)
		except IOError:
			print(_('An error occurred during the connection!\n'))
			return (False, self.PARSE_ERROR_CONN, 0)

		reply = file.readlines()
		file.close()

		response = self.parse(reply, params)

		return response


	def parse(self, reply, params):
		"""Parsing PHP submit script reply"""
		"""The response given is a list of two fields where:
		   (1) is a boolean indicator for success
		   (2) is a number indicating the action or the error type"""

		# Debug
		#print('\n', reply)
		# -----

		for line in reply:
			if line.find('Missing parameters!') == 0:
				return (False, self.PARSE_ERROR_URL)

			elif line.find('database') >= 0:
				if line.find('connect') >= 0:
					return (False, self.PARSE_ERROR_DB_CONN)
				elif line.find('select') >= 0:
					return (False, self.PARSE_ERROR_DB_SEL)

			elif line.find('Wrong password for') == 0:
				return (False, self.PARSE_ERROR_PASSWORD)

			elif line.find('User ' + params['user_id']) == 0:
				if line.find('inserted') >= 0:
					return (True, self.PARSE_INSERTED_NUNM)
			elif line.find('Machine ' + params['hostname']) == 0:
				if line.find('inserted') >= 0:
					return (True, self.PARSE_INSERTED_NM)
			elif line.find(params['bench_name'] + ' executed') == 0:
				if line.find('inserted') >= 0:
					return (True, self.PARSE_INSERTED)
				elif line.find('updated') >= 0:
					return (True, self.PARSE_UPDATED)

		return (False, self.PARSE_NOTHING)
