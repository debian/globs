## hwd.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import os

class HWDetect:
	"""Hardware detection class"""
	def __init__(self):
		self.__hostname = self.fetch_hostname()
		self.__cpu_info = self.gather_CPU_info()
		self.__gl_info = self.gather_GL_info()
		self.__mem_info = self.gather_MEM_info()


	def __getitem__(self, key):
		"""Return hardware info"""
		if key == 'host':
			return self.__hostname
		elif key == 'cpu':
			return self.__cpu_info[0]
		elif key == 'cpus':
			return self.__cpu_info
		elif key == 'gl':
			return self.__gl_info
		elif key == 'mem':
			return self.__mem_info
		else:
			raise KeyError


	def fetch_hostname(self):
		"""Fetch and store machine hostname"""
		try:
			file = open('/proc/sys/kernel/hostname')
		except IOError:
			return None

		line = file.readline()
		file.close()

		# Remove trailing '\n'
		if line[len(line)-1] == '\n':
			line = line[:len(line)-1]
		
		return line


	def gather_CPU_info(self):
		"""Gather info about the CPU"""
		try:
			file = open('/proc/cpuinfo')
		except IOError:
			return None

		lines = file.readlines()
		file.close()

		# Multi CPU support
		cpu_list = []

		for line in lines:
			if line.find('processor') == 0:
				splitted = line.split(':')
				id = int(splitted[1].strip())
				cpu = {}
				cpu_list.append(cpu)
			elif line.find('model name') == 0:
				splitted = line.split(':')
				cpu_list[id]['model'] = splitted[1].strip()
			elif line.find('cpu MHz') == 0:
				splitted = line.split(':')
				cpu_list[id]['frequency'] = splitted[1].strip()
			elif line.find('bogomips') == 0:
				splitted = line.split(':')
				cpu_list[id]['bogomips'] = splitted[1].strip()

		return cpu_list


	def gather_MEM_info(self):
		"""Gather info about the memory installed"""
		try:
			file = open('/proc/meminfo')
		except IOError:
			return None

		lines = file.readlines()
		file.close()

		for line in lines:
			if line.find('MemTotal') == 0:
				splitted = line.split(':')
				physical = splitted[1].strip()
				# removing 'kB' suffix
				physical = physical.split(' ')
				physical = physical[0].strip()
			elif line.find('SwapTotal') == 0:
				splitted = line.split(':')
				swap = splitted[1].strip()
				# removing 'kB' suffix
				swap = swap.split(' ')
				swap = swap[0].strip()

		return {'physical': physical, 'swap': swap}


	def gather_GL_info(self):
		"""Gather info about the OpenGL subsystem"""
		extensions = []
			
		pipe = os.popen('glxinfo')
		lines = pipe.readlines()
		pipe.close()

		# Default values if nothing relevant is found from glxinfo output
		vendor = _('N/A')
		renderer = _('N/A')
		version = _('N/A')
		extensions = []

		for line in lines:
			if line.find('OpenGL vendor string') == 0:
				splitted = line.split(':')
				vendor = splitted[1].strip()
			elif line.find('OpenGL renderer string') == 0:
				splitted = line.split(':')
				renderer = splitted[1].strip()
			elif line.find('OpenGL version string') == 0:
				splitted = line.split(':')
				version = splitted[1].strip()
			elif line.find('GL_') != -1: # This line contains a list of extensions
				splitted = line.split(',')
				for ext in splitted:
					if ext.find('\n') == -1:
						extensions.append(ext.strip())

		return {'vendor': vendor, 'renderer': renderer, 'version': version, 'extensions': extensions}


	def get_hostname(self):
		"""Return the hostname"""
		return self.__hostname

	def get_cpu_info(self):
		"""Return info about the first cpu detected"""
		return self.__cpu_info[0]


	def get_cpus_info(self):
		"""Return info about all the available cpus"""
		return self.__cpu_info


	def get_gl_info(self):
		"""Return the OpenGL subsystem info"""
		return self.__gl_info


	def get_mem_info(self):
		"""Return memory info"""
		return self.__mem_info
