## user.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import os
from hashlib import md5
from db import Database

class User(Database):
	"""User information managing class"""
	def __init__(self, fname):
		"""Connect and try to create the user table"""
		Database.__init__(self, fname)
		self.table = 'user'
		
		try:
			self.create_table()
		except self.error:
			print(_('User table exists already'))


	def __getitem__(self, key):
		"""Return data from the selected row of the user table"""
		SELECT = """SELECT %s FROM user;""" % key

		try:
			self.cur.execute(SELECT)
		except self.error:
			return None
	
		fetch = self.cur.fetchall()
		if len(fetch) > 0:
			return fetch[0][0]
		else:
			return None


	def create_table(self):
		"""Create the user table"""
		CREATE = """CREATE TABLE user (
			user VARCHAR(128) NOT NULL,
			password CHAR(32) NOT NULL,
			name VARCHAR(128),
			location VARCHAR(128),
	    	email VARCHAR(128),
		    homesite VARCHAR(128),
			PRIMARY KEY(user)
			);"""

		self.cur.execute(CREATE)
		self.con.commit()


	def update(self, data, is_md5 = False):
		"""Update user information"""

		if data['user'] == None or data['password'] == None\
		or data['user'] == '' or data['password'] == '':
			print(_('No value provided for user name or password!'))
			return
		
		# The password in data dictionary needs hashing
		if is_md5 == False:
			data['password'] = md5.md5(data['password']).hexdigest()

		DELETE = """DELETE FROM user;"""
		INSERT = """INSERT INTO user VALUES ('%s', '%s', '%s', '%s', '%s', '%s')""" % \
			(data['user'], data['password'], data['name'], data['location'], data['email'], data['homesite'])

		# There is only one row for the user, so first deleting then inserting
		self.cur.execute(DELETE)
		self.cur.execute(INSERT)
		self.con.commit()

