## statistics.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import array as a

try:
	import numpy as n
except ImportError:
	with_numpy = False # NumPy is not installed!
else:
	with_numpy = True


class Statistics_Numpy:
	"""Statistics class, Numpy version"""
	def __init__(self, names):
		self.chunk = 100 # Preallocation chunk size
		self.__arrays = {}
		self.__sizes = {}
		self.__max = {}
		self.__min = {}
		for name in names:
			self.__arrays[name] = n.array(n.zeros(self.chunk))
			self.__sizes[name] = 0
			self.__max[name] = None
			self.__min[name] = None


	def __getitem__(self, key):
		"""Return the results array of the selected benchmark"""
		try:
			size = self.__sizes[key]
			array = self.__arrays[key][:size]
			return array
		except KeyError:
			return None


	def __len__(self):
		"""Return the number of available benchmarks arrays"""
		return len(self.__arrays)


	def append(self, value, name):
		"""Append a value in the selected array"""
		if name not in self.__arrays:
			print(name + " " + _("doesn't exist!"))
			return None

		size = self.__sizes[name]
		array = self.__arrays[name]
		if size == len(array):
			self.__arrays[name] = n.concatenate([array, n.zeros(self.chunk)])
		self.__arrays[name][size] = value
		self.__sizes[name] += 1

		if value > self.__max[name] or self.__max[name] == None:
			self.__max[name] = value
		if value < self.__min[name] or self.__min[name] == None:
			self.__min[name] = value


	def clear(self, name):
		"""Erase the selected array"""
		try:
			self.__arrays[name] = n.array(n.zeros(self.chunk))
			self.__sizes[name] = 0
			self.__max[name] = None
			self.__min[name] = None
		except KeyError:
			print(name + " " + _("doesn't exist!"))


	def get_array(self, name, tail=0):
		"""Return the selected array, optionally just a tail of it"""
		if name not in self.__arrays:
			return None

		size = self.__sizes[name]
		array = self.__arrays[name][:size]
		if slice == 0:
			return array
		else:
			return array[-tail:]


	def get_last(self, name):
		"""Return the last value in the selected array
		   It is similar (but not the same!) to get_array(name, slice=1)"""
		if name not in self.__arrays:
			return None

		size = self.__sizes[name]
		array = self.__arrays[name][:size]
		if size == 0: # Empty array
			return None
		else:
			return array[len(array)-1]


	def get_min(self, name):
		"""Return the minimum value in the selected array"""
		try:
			return self.__min[name]
		except KeyError:
			return None


	def get_max(self, name):
		"""Return the maximum value in the selected array"""
		try:
			return self.__max[name]
		except KeyError:
			return None


	def get_avg(self, name):
		"""Return the average value in the selected array"""
		try:
			size = self.__sizes[name]
			array = self.__arrays[name][:size]
			return n.sum(array)/size
		except ZeroDivisionError:
			return 0
		except KeyError:
			return None


class Statistics_Array:
	"""Statistics class, built-in array module version"""
	def __init__(self, names):
		self.__arrays = {}
		self.__min = {}
		self.__max = {}
		for name in names:
			self.__arrays[name] = a.array('f')
			self.__max[name] = None
			self.__min[name] = None


	def __getitem__(self, key):
		"""Return the results array of the selected benchmark"""
		try:
			return self.__arrays[key].tolist()
		except KeyError:
			return None


	def __len__(self):
		"""Return the number of available benchmarks arrays"""
		return len(self.__arrays)


	def append(self, value, name):
		"""Append a value in the selected array"""
		try:
			self.__arrays[name].append(value)
			if value > self.__max[name] or self.__max[name] == None:
				self.__max[name] = value
			if value < self.__min[name] or self.__min[name] == None:
				self.__min[name] = value
		except KeyError:
			print(name + " " + _("doesn't exist!"))
			return None


	def clear(self, name):
		"""Erase the selected array"""
		try:
			del self.__arrays[name][:]
			self.__max[name] = None
			self.__min[name] = None
		except KeyError:
			print(name + " " + _("doesn't exist!"))


	def get_array(self, name, tail=0):
		"""Return the selected array, optionally just a tail of it"""
		if name not in self.__arrays:
			return None

		array = self.__arrays[name]
		if tail == 0:
			return array.tolist()
		else:
			return array[-tail:].tolist()


	def get_last(self, name):
		"""Return the last value in the selected array
		   It is similar (but not the same!) to get_array(name, slice=1)"""
		if name not in self.__arrays:
			return None

		array = self.__arrays[name]
		try:
			return array[len(array)-1]
		except IndexError: # Empty array
			return None


	def get_min(self, name):
		"""Return the minimum value in the selected array"""
		try:
			return self.__min[name]
		except KeyError:
			return None


	def get_max(self, name):
		"""Return the maximum value in the selected array"""
		try:
			return self.__max[name]
		except KeyError:
			return None


	def get_avg(self, name):
		"""Return the average value in the selected array"""
		if name not in self.__arrays:
			return None

		array = self.__arrays[name]
		try:
			avg = sum(array) / len(array)
		except ZeroDivisionError:
			avg = 0

		return avg


if with_numpy == True:
	Statistics = Statistics_Numpy
else:
	Statistics = Statistics_Array
