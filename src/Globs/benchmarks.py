## benchmarks.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import os
from subprocess import Popen, PIPE
import json #Python >= 2.6

class Benchmarks:
	"""
	This class parses every info_file inside the benchdir, then fills 
	dictionaries with options and information about the available benchmarks.
	"""
	def __init__(self, benchdir = 'benchmarks', info_file='globs_info.json'):
		self.__infos = self.gather_info(benchdir, info_file)
		self.__names = self.build_names()
		self.__options = {}

	
	def __getitem__(self, key):
		"""Return the info dictionary of the selected benchmark"""
		try:
			return self.__infos[key]
		except KeyError:
			return None


	def __len__(self):
		"""Return the number of available benchmarks"""
		return len(self.__infos)


	def gather_info(self, benchdir, infofile):
		"""Return a list with all the information collected from available benchmarks"""
		infos = {}
		execdict = {}

		try:
			benchdir_list = os.listdir(benchdir)
		except OSError:
			return infos

		for bench in benchdir_list:
			benchpath = os.path.join(benchdir, bench)
			if os.path.isdir(benchpath):
				file_list = os.listdir(benchpath)
				for f in file_list:
					if f == infofile:
						execdict.clear()
						info_file = os.path.join(benchpath, f)
						info_dicts = json.load(open(info_file))
						for k in info_dicts:
							info_dicts[k]['directory'] = benchpath
							infos[info_dicts[k]['name']] = info_dicts[k]
		return infos


	def build_names(self):
		"""Return a tuple of benchmark names"""
		names = list(self.__infos.keys())
		return names


	def exec_str(self, name):
		"""Return the string for benchmark invokation"""
		if name not in self.__infos:
			print(name + " " + _("doesn't exist!"))
			return None
		else:
			info = self.__infos[name]
			return 'cd ' + info['directory'] + ' && ./' + info['file']


	def opts_str(self, name=None, opts=None):
		"""Return the string to append for options setting"""
		if name == None and opts == None:
			print(_('opts_str() needs at least one argument!'))
			
		if name != None and name not in self.__infos:
			print(name + " " + _("doesn't exist!"))
			return None

		if opts == None:
			info = self.__infos[name]
			opts = info['defaults']

		if opts['fullscreen'] == True:
			fs = '-f '
		else:
			fs = ''

		return fs + '-w' + str(opts['width']) + ' -h' + str(opts['height']) + ' -t' + str(opts['time'])


	def check_ver(self, name, ver_str):
		"""Check if the required OpenGL version is available for the selected benchmark"""
		if name not in self.__infos:
			print(name + " " + _("doesn't exist!"))
			return None

		if 'gl_version' in self.__infos[name] and ver_str != None:
			bench_ver = self.__infos[name]['glversion']
			
			major = int(ver_str.split(' ')[0].split('.')[0])
			minor = int(ver_str.split(' ')[0].split('.')[1])
			bench_major = int(bench_ver.split('.')[0])
			if len(bench_ver.split('.')) > 1:
				bench_minor = int(bench_ver.split('.')[1])
			else:
				bench_minor = 0

			if major > bench_major:
				return True
			elif major < bench_major:
				return False
			else:
				if minor >= bench_minor:
					return True
				else:
					return False

		return True


	def check_ext(self, name, hwd_ext):
		"""Check if the required OpenGL extensions are available for the selected benchmark"""
		if name not in self.__infos:
			print(name + " " + _("doesn't exist!"))
			return None

		if 'extensions' in self.__infos[name] and hwd_ext != None and len(hwd_ext) != 0:
			bench_ext = self.__infos[name]['extensions']
			missing_ext = []
			for ext in bench_ext:
				if ext not in hwd_ext:
					missing_ext.append(ext)
			if len(missing_ext) != 0:
				for ext in missing_ext:
					print(_("OpenGL extension %s is missing!") % ext)
				return missing_ext
			else:
				return True


	def run(self, name, opts=None):
		"""Run the selected benchmark and return FPS"""
		if name not in self.__infos:
			print(name + " " + _("doesn't exist!"))
			return None

		exec_string = self.exec_str(name)
		opts_string = self.opts_str(name, opts)

		# Creating the current options dictionary
		if opts == None:
			self.__options[name] = (self.__infos[name])['defaults']
		else:
			self.__options[name] = opts

		pipe = Popen(exec_string + ' ' + opts_string, shell=True, stdout=PIPE).stdout
		output = pipe.readlines()
		pipe.close()

		fps = None
		# Parsing returned data
		for x in output:
			x = x.replace(' ', '') # stripping spaces
			x = x.replace('\n', '') # stripping newline
			splitted = x.split('=')
			if splitted[0] == 'fps':
				fps = float(splitted[1])
				return fps

		# Couldn't parse correctly the output
		else:
			if len(output):
				print(_("Could not parse benchmark output") + ':')
				for line in output:
					print("  " + line, )
				return None


	def get_info(self, name):
		"""Return the info dictionary of the selected benchmark"""
		if name not in self.__infos:
			print(name + " " + _("doesn't exist!"))
			return None
		else:
			return self.__infos[name]


	def get_names(self):
		"""Return the names tuple"""

		return self.__names


	def get_options(self, name):
		"""Return the options dictionary of the selected benchmark"""
		if name not in self.__options:
			return None
		else:
			return self.__options[name]
