## hwd_win.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import pygtk
pygtk.require('2.0')
import gtk

class HWDetect_Window:
	"""Open the detected hardware window"""
	def __init__(self, ui_file, hwd_cls):
		self.hwd = hwd_cls
		
		self.ui = gtk.Builder()
		self.ui.add_from_file(ui_file)
		app_signals = {
			"on_category_tree_button_press_event": self.on_category_tree_button_press_event,
			"on_category_tree_row_activated": self.on_category_tree_row_activated			
		}
		self.ui.connect_signals(app_signals)

		self.categories = (_('General'), _('Processor'), _('Memory'), 'OpenGL')

		# Getting widgets
		self.window = self.ui.get_object("hwd_win")
		self.hpaned1 = self.ui.get_object("hpaned1")
		self.category_tree = self.ui.get_object("category_tree")
		self.content_tree = self.ui.get_object("content_tree")

		# Init methods
		self.setup_category_tree()
		self.setup_content_tree()
		self.populate_category_tree()
		self.populate_content_tree()


	def setup_category_tree(self):
		"""Setup category tree"""

		cell = gtk.CellRendererText()

		# Columns
		column = gtk.TreeViewColumn(_('Category'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 0)
		column.set_resizable(True)
		column.set_sort_column_id(0)
		self.category_tree.append_column(column)

		self.category_tree.set_headers_clickable(False)

		# Model
		self.category_treestore = gtk.TreeStore(str)
		self.category_tree.set_model(self.category_treestore)


	def setup_content_tree(self):
		"""Setup content tree"""
		
		cell = gtk.CellRendererText()

		# Columns
		column = gtk.TreeViewColumn(_('Attribute'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 0)
		column.set_resizable(True)
		column.set_sort_column_id(0)
		self.content_tree.append_column(column)

		column = gtk.TreeViewColumn(_('Value'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 1)
		column.set_resizable(True)
		column.set_sort_column_id(1)
		self.content_tree.append_column(column)

		self.content_tree.set_headers_clickable(False)

		# Model
		self.content_treestore = {}
		for cat in self.categories:
			self.content_treestore[cat] = gtk.TreeStore(str, str)
		self.content_tree.set_model(self.content_treestore[self.categories[0]])


	def populate_category_tree(self):
		"""Populate the treeview with categories"""

		for cat in self.categories:
			self.category_treestore.append(None, [cat])


	def populate_content_tree(self):
		"""Populate content models"""

		self.content_treestore[_('General')].append(None, [_('Hostname') + ':', self.hwd['host']])

		ncpu = len(self.hwd['cpus'])
		if ncpu == 1: # Single CPU
			self.content_treestore[_('Processor')].append(None, [_('Model') + ':', self.hwd['cpu']['model']])
			self.content_treestore[_('Processor')].append(None, [_('Frequency') + ':', self.hwd['cpu']['frequency'] + ' MHz'])
			self.content_treestore[_('Processor')].append(None, ['BogoMIPS' + ':', self.hwd['cpu']['bogomips']])
		elif ncpu > 1: # Multiple CPU
			cpu_list = self.hwd['cpus']
			for cpu in cpu_list:
				cpu_iter = self.content_treestore[_('Processor')].append(None, [_('CPU') + ':', str(cpu_list.index(cpu))])
				self.content_treestore[_('Processor')].append(cpu_iter, [_('Model') + ':', cpu['model']])
				self.content_treestore[_('Processor')].append(cpu_iter, [_('Frequency') + ':', cpu['frequency'] + ' MHz'])
				self.content_treestore[_('Processor')].append(cpu_iter, ['Bogomips' + ':', cpu['bogomips']])

		self.content_treestore['OpenGL'].append(None, [_('Vendor') + ':', self.hwd['gl']['vendor']])
		self.content_treestore['OpenGL'].append(None, [_('Renderer') + ':', self.hwd['gl']['renderer']])
		self.content_treestore['OpenGL'].append(None, [_('Version') + ':', self.hwd['gl']['version']])

		self.content_treestore[_('Memory')].append(None, [_('Physical') + ':', self.hwd['mem']['physical'] + ' KB'])
		self.content_treestore[_('Memory')].append(None, [_('Swap') + ':', self.hwd['mem']['swap'] + ' KB'])


	# Callbacks
	def on_category_tree_button_press_event(self, widget, event, data=None):
		try:
			(path, column, x, y) = self.category_tree.get_path_at_pos(
				int(event.x),
				int(event.y)
			)
		except TypeError: # Returned None, no path at that position
			return

		self.category_tree.row_activated(path, self.category_tree.get_column(0))


	def on_category_tree_row_activated(self, treeview, path, view_column):
		iter = self.category_treestore.get_iter(path)
		(selname, ) = self.category_treestore.get(iter, 0)

		if selname in self.categories:
			self.content_tree.set_model(self.content_treestore[selname])
