## pref_win.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import pygtk
pygtk.require('2.0')
import gtk

class Preferences_Window:
	"""Open the preferences window"""
	def __init__(self, ui_file, user_cls):
		self.user = user_cls

		self.ui = gtk.Builder()
		self.ui.add_from_file(ui_file)
		app_signals = {
			"on_ok_button_clicked": self.on_ok_button_clicked,
			"on_cancel_button_clicked": self.on_cancel_button_clicked
		}
		self.ui.connect_signals(app_signals)

		# Getting widgets
		self.window = self.ui.get_object("pref_win")
		self.cancel_button = self.ui.get_object("cancel_button")
		self.ok_button = self.ui.get_object("ok_button")
		self.user_entry = self.ui.get_object("user_entry")
		self.password_entry = self.ui.get_object("password_entry")
		self.name_entry = self.ui.get_object("name_entry")
		self.location_entry = self.ui.get_object("location_entry")
		self.email_entry = self.ui.get_object("email_entry")
		self.homesite_entry = self.ui.get_object("homesite_entry")

		# Init methods
		self.setup_entries()


	def setup_entries(self):
		"""Setup information labels"""

		if self.user['user']:
			self.user_entry.set_text(str(self.user['user']))
		if self.user['password']:
			self.password_entry.set_text(str(self.user['password']))
		if self.user['name']:
			self.name_entry.set_text(str(self.user['name']))
		if self.user['location']:
			self.location_entry.set_text(str(self.user['location']))
		if self.user['email']:
			self.email_entry.set_text(str(self.user['email']))
		if self.user['homesite']:
			self.homesite_entry.set_text(str(self.user['homesite']))


	# Callbacks
	def on_ok_button_clicked(self, widget, data=None):
		"""Save user information in the user table via the user class"""
		data = {}
		
		data['user'] = self.user_entry.get_text()
		if self.password_entry.get_text() != self.user['password']:
			is_md5 = False
		else:
			is_md5 = True
		data['password'] = self.password_entry.get_text()
		data['name'] = self.name_entry.get_text()
		data['location'] = self.location_entry.get_text()
		data['email'] = self.email_entry.get_text()
		data['homesite'] = self.homesite_entry.get_text()
		
		self.user.update(data, is_md5)


	def on_cancel_button_clicked(self, widget, data=None):
		"""Close the preferences window"""
 		self.window.destroy()
