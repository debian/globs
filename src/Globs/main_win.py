## main_win.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import sys
import os
import pygtk
pygtk.require('2.0')
import gtk
import webbrowser

import about_dlg, hwd_win, pref_win, local_db_win, stats_area
import Globs

class Main_Window:
	def __init__(self, app_cls):
		"""Handle the main window"""
		# Getting instances of "singleton" classes
		self.db = app_cls.db
		self.bm = app_cls.bm
		self.stats = app_cls.stats
		self.user = app_cls.user
		self.hwd = app_cls.hwd
		self.submit = app_cls.submit
		
		self.share_dir = app_cls.share_dir
		self.ui_dir = app_cls.ui_dir

		self.ui = gtk.Builder()
		self.ui.add_from_file(os.path.join(self.ui_dir, 'main_win.ui'))

		app_signals = {
			"on_main_win_destroy": self.on_main_win_destroy,
			"on_detected_hw_activate": self.on_detected_hw_activate,
			"on_preferences_activate": self.on_preferences_activate,
			"on_local_db_activate": self.on_local_db_activate,
			"on_clear_activate": self.on_clear_activate,
			"on_purge_activate": self.on_purge_activate,
			"on_dump_activate": self.on_dump_activate,
			"on_show_remote_activate": self.on_show_remote_activate,
			"on_quit_activate": self.on_quit_activate,
			"on_about_activate": self.on_about_activate,
			"on_help_activate": self.on_help_activate,
			"on_bench_list_button_press_event": self.on_bench_list_button_press_event,
			"on_bench_list_row_activated": self.on_bench_list_row_activated,
			"on_execute_button_clicked": self.on_execute_button_clicked,
			"on_res_combobox_changed": self.on_res_combobox_changed,
			"on_lock_check_toggled": self.on_lock_check_toggled
		}
		self.ui.connect_signals(app_signals)
		
		# Getting widgets
		self.window = self.ui.get_object("main_win")
		self.bench_list = self.ui.get_object("bench_list")
		
		# Informations
		self.info_textview = self.ui.get_object("info_textview")
		
		# Options
		self.options_frame = self.ui.get_object("options_frame")
		self.res_combobox = self.ui.get_object("res_combobox")
		self.width_spin = self.ui.get_object("width_spin")
		self.height_spin = self.ui.get_object("height_spin")
		self.time_spin = self.ui.get_object("time_spin")
		self.repeat_spin = self.ui.get_object("repeat_spin")
		self.fs_check =  self.ui.get_object("fs_check")
		self.lock_check = self.ui.get_object("lock_check")

		# Statistics
		self.statsa = stats_area.Statistics_Area(self.stats)
		self.stats_vbox = self.ui.get_object("stats_vbox")
		
		self.statusbar = self.ui.get_object("statusbar")


		# Setting initial attributes
		pix = gtk.gdk.pixbuf_new_from_file(os.path.join(self.share_dir, 'pixmaps/globs.png'))
		gtk.window_set_default_icon(pix)
		self.window.set_icon(pix)

		self.info_buffer = self.ui.get_object("info_buffer")
		self.boldtag = self.info_buffer.create_tag('bold')
		self.boldtag.set_property('weight', 700)
		self.info_buffer.insert_with_tags(self.info_buffer.get_end_iter(), _('Name:') + ' ', self.boldtag)
		self.info_buffer.insert(self.info_buffer.get_end_iter(), _('N/A') + '\n')
		self.info_buffer.insert_with_tags(self.info_buffer.get_end_iter(), _('Author:') + ' ', self.boldtag)
		self.info_buffer.insert(self.info_buffer.get_end_iter(), _('N/A') + '\n')
		self.info_buffer.insert_with_tags(self.info_buffer.get_end_iter(), _('Description:') + ' ', self.boldtag)
		self.info_buffer.insert(self.info_buffer.get_end_iter(), _('N/A') + '\n')

		# Open/close window flags
		self.hwd_win = None
		self.pref_win = None
		self.local_db_win = None
		
		# Init methods
		self.setup_bench_list()
		self.setup_res_combobox()
		self.populate_bench_list()
		self.statsa.setup(self.stats_vbox)


	def setup_bench_list(self):
		"""Setup the treeview for benchmarks list"""
		cell = gtk.CellRendererText()

		# Columns
		column = gtk.TreeViewColumn(_('Name'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 0)
		column.set_resizable(True)
		column.set_sort_column_id(0)
		self.bench_list.append_column(column)
		
		column = gtk.TreeViewColumn(_('Version'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 1)
		column.set_resizable(True)
		column.set_sort_column_id(1)
		self.bench_list.append_column(column)

		column = gtk.TreeViewColumn(_('Best FPS'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 2)
		column.set_resizable(True)
		column.set_sort_column_id(2)
		self.bench_list.append_column(column)

		# Model
		self.liststore = gtk.ListStore(str, str, float)
		self.bench_list.set_model(self.liststore)
		# Selection Mode
		selection = self.bench_list.get_selection()
		selection.set_mode(gtk.SELECTION_MULTIPLE)


	def setup_res_combobox(self):
		"""Setup the resolutions combobox entry under the options tab"""

		liststore = gtk.ListStore(str)
		liststore.insert(0, (_('Custom'), ))
		liststore.insert(1, ('1280x1024', ))
		liststore.insert(2, ('1024x768', ))
		liststore.insert(3, ('800x600', ))
		liststore.insert(4, ('640x480', ))
		cell = gtk.CellRendererText()
		self.res_combobox.pack_start(cell, True)
		self.res_combobox.add_attribute(cell, 'text', 0)
		self.res_combobox.set_model(liststore)
		self.res_combobox.set_active(1)


	def populate_bench_list(self):
		"""Populate the treeview with benchmarks"""
		for name in self.bm.get_names():
			info = self.bm.get_info(name)
			self.liststore.append([info['name'], info['version'], 0])


	def get_options(self):
		"""Return an options dictionary from the options tab"""
		opts = {}
		
		opts['width'] = int(self.width_spin.get_value())
		opts['height'] = int(self.height_spin.get_value())
		opts['time'] = int(self.time_spin.get_value())
		opts['repetitions'] = int(self.repeat_spin.get_value())
		opts['fullscreen'] = self.fs_check.get_active()
																	
		return opts


	def update_stats(self, name):
		"""Update the statistics tab"""
		if name in self.bm.get_names() == False:
			print (name + " " + _("doesn't exist!"))
			return None

		self.statsa.update(name)


	# Callbacks
	def on_main_win_destroy(self, widget, data=None):
		"""Quit the application"""
		return gtk.main_quit()


	def on_quit_activate(self, event):
		"""Quit the application"""
		return gtk.main_quit()


	def on_detected_hw_activate(self, event):
		"""Open the detected hardware window"""
		if self.hwd_win:
			self.hwd_win.window.present()
		else:
			ui_file = os.path.join(self.ui_dir, 'hwd_win.ui')
			self.hwd_win = hwd_win.HWDetect_Window(ui_file, self.hwd)
			self.hwd_win.window.connect('destroy', self.on_detected_hw_destroy)


	def on_detected_hw_destroy(self, event):
		"""Close the detected hardware window"""
		self.hwd_win = None


	def on_preferences_activate(self, event):
		"""Open the preferences window"""
		if self.pref_win:
			self.pref_win.window.present()
		else:
			ui_file = os.path.join(self.ui_dir, 'pref_win.ui')
			self.pref_win = pref_win.Preferences_Window(ui_file, self.user)
			self.pref_ok_sig_id = self.pref_win.ok_button.connect("clicked", self.on_preferences_button_clicked)
			self.pref_cancel_sig_id = self.pref_win.cancel_button.connect("clicked", self.on_preferences_button_clicked)
			self.pref_win.window.connect('destroy', self.on_preferences_destroy)


	def on_preferences_destroy(self, event):
		"""Close the preferences window"""
		self.pref_win = None


	def on_preferences_button_clicked(self, event):
		"""Destroy the preferences window either cancel or ok button is clicked"""
		self.pref_win.ok_button.disconnect(self.pref_ok_sig_id)
		self.pref_win.cancel_button.disconnect(self.pref_cancel_sig_id)
		
		self.pref_win.window.destroy()


	def on_local_db_activate(self, event):
		"""Open the local database window"""
		if self.local_db_win:
			self.local_db_win.window.present()
		else:
			ui_file = os.path.join(self.ui_dir, 'local_db_win.ui')
			self.local_db_win = local_db_win.Local_DB_Window(ui_file, self.db, self.bm, self.user, self.submit)
			self.local_db_win.window.connect('destroy', self.on_local_db_destroy)


	def on_local_db_destroy(self, event):
		"""Close the local database window"""
		self.local_db_win = None


	def on_purge_activate(self, event):
		"""Purge the database from non existant benchmarks"""

		dlg = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO,
			message_format=_('Do you really want to purge the database?'))
		dlg.set_default_response(gtk.RESPONSE_NO)
		response = dlg.run()
		dlg.destroy()

		if response == gtk.RESPONSE_NO:
			return
		else:
			self.db.purge(self.bm.get_names())
			self.statusbar.push(1, _('Database purged'))


	def on_clear_activate(self, event):
		"""Clear the whole database"""

		dlg = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO,
			message_format=_('Do you really want to clear the database?'))
		dlg.set_default_response(gtk.RESPONSE_NO)
		response = dlg.run()
		dlg.destroy()

		if response == gtk.RESPONSE_NO:
			return
		else:
			self.db.clear()
			self.statusbar.push(1, _('Database cleared'))


	def on_dump_activate(self, event):
		"""Dump the local database in a text file"""

		dlg = gtk.FileChooserDialog('DB Dump', self.window, gtk.FILE_CHOOSER_ACTION_SAVE, 
			(gtk.STOCK_OK, gtk.RESPONSE_ACCEPT, gtk.STOCK_CANCEL, gtk.RESPONSE_REJECT))
		dlg.set_default_response(gtk.RESPONSE_ACCEPT)
		response = dlg.run()

		if response == gtk.RESPONSE_ACCEPT:
			file = dlg.get_filename()
		else:
			dlg.destroy()
			return
		
		dlg.destroy()

		self.db.save_as(file)
		self.statusbar.push(1, _('Database saved as ') + file)


	def on_show_remote_activate(self, event):
		"""Open the browser on the remote database browsing page"""
		webbrowser.open_new_tab(Globs.BROWSE_URL)


	def on_about_activate(self, event):
		"""Open the about dialog"""
		ui_file = os.path.join(self.ui_dir, 'about_dlg.ui')
		about_dlg.About_Dialog(ui_file)
		return


	def on_help_activate(self, event):
		"""Open the browser on the online help page"""
		webbrowser.open_new_tab(Globs.HELP_URL)


	def on_bench_list_button_press_event(self, widget, event, data=None):
		"""A treeview element was clicked once"""
		try:
			(path, column, x, y) = self.bench_list.get_path_at_pos(
				int(event.x),
				int(event.y)
			)
		except TypeError: # Returned None, no path at that position
			return

		self.bench_list.row_activated(path, self.bench_list.get_column(0))


	def on_bench_list_row_activated(self, treeview, path, view_column):
		"""A treeview element was activated"""
		iter = self.liststore.get_iter(path)
		(selname, ) = self.liststore.get(iter, 0)

		for name in self.bm.get_names():
			info = self.bm.get_info(name)
			if info['name'] == selname:
				opts = info['defaults']
				self.info_buffer.set_text('')
				self.info_buffer.insert_with_tags(self.info_buffer.get_end_iter(), _('Name:') + ' ', self.boldtag)
				self.info_buffer.insert(self.info_buffer.get_end_iter(), info['name'] + '\n')
				self.info_buffer.insert_with_tags(self.info_buffer.get_end_iter(), _('Author:') + ' ', self.boldtag)
				self.info_buffer.insert(self.info_buffer.get_end_iter(), info['author'] + '\n')
				self.info_buffer.insert_with_tags(self.info_buffer.get_end_iter(), _('Description:') + ' ', self.boldtag)
				self.info_buffer.insert(self.info_buffer.get_end_iter(), info['description'] + '\n')

				if self.lock_check.get_active() == False:
					self.res_combobox.set_active(0)
					self.width_spin.set_value(opts['width'])
					self.height_spin.set_value(opts['height'])
					self.time_spin.set_value(opts['time'])
					self.repeat_spin.set_value(1)
					self.fs_check.set_active(opts['fullscreen'])

				self.update_stats(name)


	def on_execute_button_clicked(self, widget, data=None):
		"""Execute button was clicked, executing selected benchmarks"""

		selection = self.bench_list.get_selection()
		(model, paths) = selection.get_selected_rows()
		if len(paths) == 0:
			return
		elif len(paths) > 1: # multi-selection
			dlg = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO,
				message_format=_('Do you really want to execute the selected benchmarks?'))
			dlg.set_default_response(gtk.RESPONSE_YES)
			response = dlg.run()
			dlg.destroy()
			if response == gtk.RESPONSE_NO:
				return

		for path in paths:
			iter = model.get_iter(path)
			name = model.get_value(iter, 0)
		
			if name not in self.bm.get_names():
				continue

			#self.statusbar.push(1, _('Executing ') + name + '...')
			info = self.bm.get_info(name)
			opts = self.get_options()
			if 'repetitions' not in opts:
				opts['repetitions'] = 1

			ver_str = self.hwd.get_gl_info()['version']
			if self.bm.check_ver(name, ver_str) == False:
				dlg = gtk.MessageDialog(type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_CLOSE,
					message_format="OpenGL " + self.bm.get_info(name)['glversion'] + _(" is not available"))
				dlg.set_default_response(gtk.RESPONSE_CLOSE)
				dlg.run()
				dlg.destroy()
				return

			hwd_ext = self.hwd.get_gl_info()['extensions']
			ext_list = self.bm.check_ext(name, hwd_ext)
			if ext_list.__class__ == list: # Returned a list of missing extensions
				missing_ext = ''
				for ext in ext_list:
					missing_ext += ext
					if ext_list.index(ext) != len(ext_list) - 1:
						missing_ext += ', '
				dlg = gtk.MessageDialog(type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_CLOSE,
					message_format=_("You're missing the following extensions: " + missing_ext))
				dlg.set_default_response(gtk.RESPONSE_CLOSE)
				dlg.run()
				dlg.destroy()
				return

			for i in range(opts['repetitions']): # Have been already executed one time
				fps = self.bm.run(name, opts)
				
				if fps != None:
					date = self.db.insert(info, opts, fps) # Inserting in the database
					self.stats.append(fps, name) # Appending in the statistics
					self.statusbar.push(1, name + _(' executed at ') + str(fps) + ' fps')
					model.set_value(iter, 2, self.stats.get_max(name))
					self.update_stats(name)


	def on_res_combobox_changed(self, widget, data=None):
		"""Setting width and height spin buttons from the combobox"""

		iter = self.res_combobox.get_active_iter()
		model = self.res_combobox.get_model()
		resolution = model.get_value(iter, 0)

		if resolution == _('Custom'):
			self.width_spin.set_sensitive(True)
			self.height_spin.set_sensitive(True)
		else:
			self.width_spin.set_sensitive(False)
			self.height_spin.set_sensitive(False)
			width = int(resolution.split('x')[0])
			height = int(resolution.split('x')[1])
			self.width_spin.set_value(width)
			self.height_spin.set_value(height)


	def on_lock_check_toggled(self, widget, data=None):
		"""Locking or releasing options based on check button"""
		if self.lock_check.get_active():
			self.options_frame.set_sensitive(False)
		else:
			self.options_frame.set_sensitive(True)
