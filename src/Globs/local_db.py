## local_db.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import os
from db import Database

class Local_Database(Database):
	"""Internal benchmarks database managing class"""
	def __init__(self, fname):
		"""Connect and try to create the benchmark table"""
		Database.__init__(self, fname)
		self.table = 'benchmarks'

		try:
			self.create_table()
		except self.error:
			print(_('Benchmarks table exists already'))


	def create_table(self):
		"""Create the benchmark table"""
		CREATE = """CREATE TABLE benchmarks (
			name VARCHAR(128) NOT NULL,
			version VARCHAR(128) NOT NULL,
			fps FLOAT NOT NULL,
			width INTEGER,
			height INTEGER,
			fullscreen BOOLEAN,
			time INTEGER,
			date TIMESTAMP NOT NULL,
			PRIMARY KEY(date)
			);"""
	
		self.cur.execute(CREATE)
		self.con.commit()


	def delete(self, bench_name):
		"""Delete a row in the benchmark table"""
		DELETE = """DELETE FROM benchmarks WHERE name='%s';""" % bench_name
		
		self.cur.execute(DELETE)
		self.con.commit()


	def insert(self, info, opts, fps):
		"""Insert a row in the benchmark table if the new fps result is better than the one which is currently stored"""

		DATETIME = """SELECT DATETIME('NOW', 'LOCALTIME');"""
		self.cur.execute(DATETIME)
		fetch = self.cur.fetchall()
		now = fetch[0][0]

		INSERT = """INSERT INTO benchmarks VALUES ('%s', '%s', %s, %s, %s, '%s', %s, '%s');""" % (info['name'], info['version'], fps, opts['width'], opts['height'], opts['fullscreen'], opts['time'], now)
		UPDATE = """UPDATE benchmarks SET version='%s', fps=%s, time=%s, date='%s' """ % (info['version'], fps, opts['time'], now)
		CONDITION_MIN = """WHERE name='%s' AND width='%s' AND height='%s' AND fullscreen='%s' AND time<='%s' AND fps<'%s';""" % (info['name'],  opts['width'], opts['height'], opts['fullscreen'], opts['time'], fps)
		CONDITION_MAJ = """WHERE name='%s' AND width='%s' AND height='%s' AND fullscreen='%s' AND time<='%s' AND fps>='%s';""" % (info['name'],  opts['width'], opts['height'], opts['fullscreen'], opts['time'], fps)
		COUNT_MAJ = """SELECT COUNT(*) FROM benchmarks """ + CONDITION_MAJ
		COUNT_MIN = """SELECT COUNT(*) FROM benchmarks """ + CONDITION_MIN

		self.cur.execute(COUNT_MIN)
		fetch = self.cur.fetchall()
		counter_min = fetch[0][0]

		self.cur.execute(COUNT_MAJ)
		fetch = self.cur.fetchall()
		counter_maj = fetch[0][0]
				
		if counter_min != 0: # There are worse results needed to be upgraded
			update = True;
		else:
			update = False;

		modified = False
		# INSERT
		if ((update == False and counter_maj == 0) or
			(counter_min == 0 and counter_maj == 0)): # update condition false, but because there
			self.cur.execute(INSERT)
			modified = True
		elif update == True:
			self.cur.execute(UPDATE + CONDITION_MIN)
			modified = True

		self.con.commit()

		if modified:
			return now
		else:
			return None


	def remove(self, date):
		"""Remove the benchmark executed on 'date' from the database"""
		DELETE = """DELETE FROM benchmarks WHERE date='%s'""" % date

		self.cur.execute(DELETE)
		self.con.commit()


	def purge(self, bench_names):
		"""Delete rows which are NOT in the bench_names tuple"""
		DELETE = """DELETE FROM benchmarks WHERE name not in %s;""" % str(bench_names)

		# Convert a string rep of a list in a string rep of a tuple
		DELETE = DELETE.replace('[', '(')
		DELETE = DELETE.replace(']', ')')

		self.cur.execute(DELETE)
		self.con.commit()
