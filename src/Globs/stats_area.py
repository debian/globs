## stats_area.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

try:
	import matplotlib.figure as f
	import matplotlib.pylab as p
	import numpy as n
	from matplotlib.backends.backend_gtkcairo import FigureCanvasGTKCairo as FigureCanvas
except ImportError:
	with_matplotlib = False # Matplotlib and NumPy are not installed!
	import gtk
else:
	with_matplotlib = True


class Statistics_Area_Chart:
	"""Statistics Drawing Area class, Chart version"""
	def __init__(self, statistics):
		self.stats = statistics

	def setup(self, parent_box):
		"""Setup the Drawing Area class for the bar chart"""
		self.parent = parent_box
		self.idx = 5 # Show only the last idx results

		self.fig = f.Figure(figsize=(5,4), dpi=100, facecolor='w', edgecolor='k')
		self.fig.hold(False)
		self.axes = self.fig.add_subplot(111)
		self.axes.grid(True)
		self.axes.set_xlim(0, self.idx + 4) # Last idx elements plus empty, max, min and avg
		self.axes.set_xticklabels([], fontsize=8)
		self.axes.set_yticklabels([], fontsize=8)

		self.canvas = FigureCanvas(self.fig)  # a gtk.DrawingArea
		self.parent.pack_start(self.canvas, True, True)
		self.canvas.show()


	def update(self, name):
		"""Update the bar chart with the last collected data"""
		idx = self.idx
		height = self.stats.get_array(name, idx)
		labels = []
		for h in height:
			labels.append(str(h))
		sz = len(height)

		last = self.stats.get_last(name)
		max = self.stats.get_max(name)
		avg = self.stats.get_avg(name)
		min = self.stats.get_min(name)

		self.axes.clear()
		self.axes.grid(True)
		if sz >= 1: # There's at least one result
			if sz >= 2: # There're at least two results
				self.axes.bar(n.arange(0.1, sz-1), height[:-1], 
					xerr = n.arange(0.1, sz-1), yerr = n.zeros(sz-1), color='b') # Previous history
			self.axes.bar(0.1 + sz - 1, last, xerr=0.1 + sz - 1, yerr=0, color='c') # Last
			self.axes.bar(idx + 1 + 0.1, max, xerr=idx + 0.1, yerr=0, color='g') # Max
			self.axes.bar(idx + 2 + 0.1, avg, xerr=idx + 1 + 0.1, yerr=0, color='y') # Avg
			self.axes.bar(idx + 3 + 0.1, min, xerr=idx + 2 + 0.1, yerr=0, color='r') # Min
			self.axes.set_xlim(0, idx + 4) # Last idx elements plus empty, max, min and avg
			self.axes.set_xticks(n.concatenate((n.arange(0.1 + 0.4, sz), n.arange(idx + 1 + 0.1 + 0.4, idx + 4))))
			self.axes.set_xticklabels(labels + [str(max), str(avg), str(min)], fontsize=8)
		else: # Empty chart
			self.axes.set_xlim(0, idx + 4) # Last idx elements plus max, min and avg
			self.axes.set_xticklabels([])
			self.axes.set_yticklabels([])

		self.canvas.draw()


class Statistics_Area_Simple:
	"""Statistics Drawing Area class, Simple version"""
	def __init__(self, statistics):
		self.stats = statistics


	def setup(self, parent_box):
		"""Setup the progress bars for *poor man* charts"""
		self.parent = parent_box

		hbox = gtk.HBox(False, 0)
		vbox_r = gtk.VBox(True, 0)
		vbox_l = gtk.VBox(True, 0)
		hbox.pack_start(vbox_r, False, True, 0)
		hbox.pack_start(vbox_l, True, True, 0)

		lab1 = gtk.Label(_('Best FPS:'))
		lab2 = gtk.Label(_('Last FPS:'))
		lab3 = gtk.Label(_('Worst FPS:'))
		vbox_r.pack_start(lab1, False, False, 0)
		vbox_r.pack_start(lab2, False, False, 0)
		vbox_r.pack_start(lab3, False, False, 0)

		self.best_bar = gtk.ProgressBar()
		self.best_bar.set_text(_('N/A'))
		self.best_bar.set_fraction(0.0)
		self.last_bar = gtk.ProgressBar()
		self.last_bar.set_text(_('N/A'))
		self.last_bar.set_fraction(0.0)
		self.worst_bar = gtk.ProgressBar()
		self.worst_bar.set_text(_('N/A'))
		self.worst_bar.set_fraction(0.0)
		vbox_l.pack_start(self.best_bar, False, False, 0)
		vbox_l.pack_start(self.last_bar, False, False, 0)
		vbox_l.pack_start(self.worst_bar, False, False, 0)

		self.parent.pack_start(hbox)
		hbox.show_all()


	def update(self, name):
		"""Update the progress bars with the last collected data"""
		max = self.stats.get_max(name)
		last = self.stats.get_last(name)
		min = self.stats.get_min(name)

		if max != None:
			self.best_bar.set_text(str(max))
			self.best_bar.set_fraction(1.0)
		else:
			self.best_bar.set_text(_('N/A'))
			self.best_bar.set_fraction(0.0)

		if last != None:
			self.last_bar.set_text(str(last))
			self.last_bar.set_fraction(last/max)
		else:
			self.last_bar.set_text(_('N/A'))
			self.last_bar.set_fraction(0.0)

		if min != None:
			self.worst_bar.set_text(str(min))
			self.worst_bar.set_fraction(min/max)
		else:
			self.worst_bar.set_text(_('N/A'))
			self.worst_bar.set_fraction(0.0)


if with_matplotlib == True:
	Statistics_Area = Statistics_Area_Chart
else:
	Statistics_Area = Statistics_Area_Simple
