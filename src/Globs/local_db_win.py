## local_db_win.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import pygtk
pygtk.require('2.0')
import gtk

class Local_DB_Window:
	"""Open the local database window"""
	def __init__(self, ui_file, db_cls, bm_cls, user_cls, sub_cls):
		# Saving instances of "singleton" classes
		self.db = db_cls
		self.bm = bm_cls
		self.user = user_cls
		self.submit = sub_cls

		self.ui = gtk.Builder()
		self.ui.add_from_file(ui_file)
		app_signals = {
			"on_submit_entry_button_clicked": self.on_submit_entry_button_clicked,
			"on_delete_entry_button_clicked": self.on_delete_entry_button_clicked,
			"on_refresh_button_clicked": self.on_refresh_button_clicked,
			"on_close_button_clicked": self.on_close_button_clicked
		}
		self.ui.connect_signals(app_signals)

		# Getting widgets
		self.window = self.ui.get_object("local_db_win")
		self.bench_list = self.ui.get_object("bench_list")
		self.close_button = self.ui.get_object("close_button")

		# Init methods
		self.setup_bench_list()
		self.populate_bench_list()


	def setup_bench_list(self):
		"""Setup the treeview for benchmarks list"""
		cell = gtk.CellRendererText()

			
		# Columns
		column = gtk.TreeViewColumn(_('Name'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 0)
		column.set_resizable(True)
		column.set_sort_column_id(0)
		self.bench_list.append_column(column)
		
		column = gtk.TreeViewColumn(_('Version'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 1)
		column.set_resizable(True)
		column.set_sort_column_id(1)
		self.bench_list.append_column(column)

		column = gtk.TreeViewColumn(_('FPS'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 2)
		column.set_resizable(True)
		column.set_sort_column_id(2)
		self.bench_list.append_column(column)

		column = gtk.TreeViewColumn(_('Width'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 3)
		column.set_resizable(True)
		column.set_sort_column_id(3)
		self.bench_list.append_column(column)

		column = gtk.TreeViewColumn(_('Height'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 4)
		column.set_resizable(True)
		column.set_sort_column_id(4)
		self.bench_list.append_column(column)

		column = gtk.TreeViewColumn(_('Fullscreen'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 5)
		column.set_resizable(True)
		column.set_sort_column_id(5)
		self.bench_list.append_column(column)

		column = gtk.TreeViewColumn(_('Time'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 6)
		column.set_resizable(True)
		column.set_sort_column_id(6)
		self.bench_list.append_column(column)

		column = gtk.TreeViewColumn(_('Date'))
		column.pack_start(cell, True)
		column.add_attribute(cell, 'text', 7)
		column.set_resizable(True)
		column.set_sort_column_id(7)
		self.bench_list.append_column(column)

		# Model
		self.liststore = gtk.ListStore(str, str, float, int, int, str, int, str)
		self.bench_list.set_model(self.liststore)
		# Selection Mode
		selection = self.bench_list.get_selection()
		selection.set_mode(gtk.SELECTION_MULTIPLE)


	def populate_bench_list(self):
		"""Populate the treeview with benchmarks"""
		rows = self.db.select()

		for row in rows:
			self.liststore.append(row)


	def update_bench_list(self):
		"""Update the benchmarks treeview"""

		self.liststore.clear()
		self.populate_bench_list()


	# Callbacks
	def on_submit_entry_button_clicked(self, event):
		"""Submit selected entries to the common online database"""

		if self.user['user'] == None or self.user['password'] == None:
			dlg = gtk.MessageDialog(type=gtk.MESSAGE_ERROR, buttons=gtk.BUTTONS_CLOSE,
				message_format=_('You should provide a user name and a password in the User tab of the Preferences window!'))
			dlg.set_default_response(gtk.RESPONSE_CLOSE)
			response = dlg.run()
			dlg.destroy()
			return

		selection = self.bench_list.get_selection()
		(model, paths) = selection.get_selected_rows()
		if len(paths) == 0:
			return
		elif len(paths) > 1: # multi-selection
			dlg = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO,
				message_format=_('Do you really want to submit the selected entries?'))
			dlg.set_default_response(gtk.RESPONSE_YES)
			response = dlg.run()
			dlg.destroy()
			if response == gtk.RESPONSE_NO:
				return

		message = ''
		for path in paths:
			iter = model.get_iter(path)
			name = model.get_value(iter, 0)
			date = model.get_value(iter, 7)

			response = self.submit.send(date)
			if response[0] == False: # Benchmark neither inserted nor updated
				dialog_type = gtk.MESSAGE_ERROR
				if response[1] == self.submit.PARSE_ERROR_CONN:
					message = _('Error in the connection!')
					break
				elif response[1] == self.submit.PARSE_ERROR_URL:
					message = _('Malformed URL sent!')
					break
				elif response[1] == self.submit.PARSE_ERROR_DB_CONN:
					message = _('Cannot connect to the remote database!')
					break
				elif response[1] == self.submit.PARSE_ERROR_DB_SEL:
					message = _('The remote database cannot be selected!')
					break
				elif response[1] == self.submit.PARSE_ERROR_PASSWORD:
					message = _('Wrong password!')
					break
				elif response[1] == self.submit.PARSE_NOTHING:
					dialog_type = gtk.MESSAGE_INFO
					message += _(name + ' not updated\n')
			else: # benchmark either inserted or updated
				dialog_type = gtk.MESSAGE_INFO
				if response[1] == self.submit.PARSE_UPDATED:
					message += _(name + ' updated\n')
				elif response[1] == self.submit.PARSE_INSERTED:
					message += _(name + ' inserted\n')
				elif response[1] == self.submit.PARSE_INSERTED_NM:
					message = _('New machine inserted\n')
					message += _(name + ' inserted\n')
				elif response[1] == self.submit.PARSE_INSERTED_NUNM:
					message = _('New user and new machine inserted\n')
					message += _(name + ' inserted\n')


		dlg = gtk.MessageDialog(type=dialog_type, buttons=gtk.BUTTONS_CLOSE, message_format=message)
		dlg.set_default_response(gtk.RESPONSE_CLOSE)
		dlg.run()
		dlg.destroy()


	def on_delete_entry_button_clicked(self, event):
		"""Delete selected entries from the database"""

		selection = self.bench_list.get_selection()
		(model, paths) = selection.get_selected_rows()
		if len(paths) == 0:
			return
		elif len(paths) > 1: # multi-selection
			dlg = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION, buttons=gtk.BUTTONS_YES_NO,
				message_format=_('Do you really want to delete the selected entries?'))
			dlg.set_default_response(gtk.RESPONSE_YES)
			response = dlg.run()
			dlg.destroy()
			if response == gtk.RESPONSE_NO:
				return

		paths.reverse() # Removing iters in reverse order
		for path in paths:
			iter = model.get_iter(path)
			date = model.get_value(iter, 7)

			self.db.remove(date)
			model.remove(iter)


	def on_refresh_button_clicked(self, event):
		"""Refresh entries listing"""

		self.update_bench_list()


	def on_close_button_clicked(self, event):
		"""Close the local database window"""
		self.window.destroy()
