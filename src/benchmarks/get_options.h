#ifndef GET_OPTIONS_H
#define GET_OPTIONS_H

typedef enum {OPT_FALSE=0, OPT_TRUE} boolean;
typedef enum {UNKNOWN_ERR=-2, NOVALUE_ERR=-1, NO_ERR=0} error_cond;

typedef struct options_s
{
	boolean fullscreen; /* Fullscreen flag */
	int width; /* Screen's width*/
	int height; /* Screen's height */
	int time; /* Bnechmark running time */
} options;

#endif

#define DEFAULT_WIDTH 640
#define DEFAULT_HEIGHT 480
#define DEFAULT_TIME 5

error_cond get_options(int argc, char **argv, options *opt);
