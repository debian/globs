#include <unistd.h>
#include <stdio.h>

#include "../get_options.h"
#include "../check_time.h"

int main(int argc, char **argv)
{

	options opt;
	error_cond err;

/* ----- Gathering options ---------- */
	err = get_options(argc, argv, &opt);


/* ----- Main cycle (frame sync) -- */
	while(1) {
		if (check_time(opt.time))
			break;
	}


/* ----- Quitting ----------------- */
	/* printf("f = %d\nw = %d\nh = %d\nt = %d\n", opt.fullscreen, opt.width, opt.height, opt.time); */
	return err;

}
