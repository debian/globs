/* GLSL_parallax: Parallax mapping with GLSL shaders
   Copyright (C) 2007 Angelo "Encelo" Theodorou
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#include <stdio.h>
#include <stdlib.h>

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glext.h> 

#include "init.h"
#include "textfile.h"


void InitLighting(struct Light *l, struct Material *m)
{
	glMaterialfv(GL_FRONT, GL_AMBIENT_AND_DIFFUSE, m->mat_diffuse);
	glMaterialfv(GL_FRONT, GL_SPECULAR, m->mat_specular);
	glMaterialfv(GL_FRONT, GL_SHININESS, m->mat_shininess);
	glLightfv(GL_LIGHT0, GL_AMBIENT, l->ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, l->diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, l->specular);
	glLightfv(GL_LIGHT0, GL_POSITION, l->position);
	glLightf(GL_LIGHT0, GL_CONSTANT_ATTENUATION, 1.5);
	glLightf(GL_LIGHT0, GL_LINEAR_ATTENUATION, 0.5);
	glLightf(GL_LIGHT0, GL_QUADRATIC_ATTENUATION, 0.25);

	glEnable(GL_COLOR_MATERIAL);
	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
}


void InitShaders(struct Shader *sh)
{
	char *vs, *fs;

	sh->p = glCreateProgram();

	sh->v = glCreateShader(GL_VERTEX_SHADER);
	vs = textFileRead("shader.vert");
	const char *vv = vs;
	glShaderSource(sh->v, 1, &vv, NULL);
	free(vs);
	glCompileShader(sh->v);
	glAttachShader(sh->p, sh->v);
	
	sh->f = glCreateShader(GL_FRAGMENT_SHADER);
	fs = textFileRead("shader.frag");
	const char *ff = fs;
	glShaderSource(sh->f, 1, &ff, NULL);
	free(fs);
	glCompileShader(sh->f);
	glAttachShader(sh->p, sh->f);

	glLinkProgram(sh->p);
	printObjectInfoLog(sh->v);
	printObjectInfoLog(sh->f);
	printObjectInfoLog(sh->p);
	glUseProgram(sh->p);

	sh->tex0 = glGetUniformLocation(sh->p, "Tex0");
	sh->tex1 = glGetUniformLocation(sh->p, "Tex1");
	sh->tex2 = glGetUniformLocation(sh->p, "Tex2");
	glUniform1i(sh->tex0, 0);
	glUniform1i(sh->tex1, 1);
	glUniform1i(sh->tex2, 2);
}


void InitTextures(GLuint texid[])
{
	SDL_Surface *image;

	if (!(image = (SDL_Surface *) IMG_Load("colormap.png"))) {
		printf("Color map load error");
		exit(-1);
	}

	glActiveTexture(GL_TEXTURE0);
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texid[0]);
	glBindTexture(GL_TEXTURE_2D, texid[0]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	SDL_FreeSurface(image);


	if (!(image = (SDL_Surface *) IMG_Load("parallaxmap.png"))) {
		printf("Parallax map load error");
		exit(-1);
	}

	glActiveTexture(GL_TEXTURE1);
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texid[1]);
	glBindTexture(GL_TEXTURE_2D, texid[1]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->w, image->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->pixels);
	SDL_FreeSurface(image);


	if (!(image = (SDL_Surface *) IMG_Load("glossmap.png"))) {
		printf("Gloss map load error");
		exit(-1);
	}

	glActiveTexture(GL_TEXTURE2);
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texid[2]);
	glBindTexture(GL_TEXTURE_2D, texid[2]);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->w, image->h, 0, GL_RGB, GL_UNSIGNED_BYTE, image->pixels);
	SDL_FreeSurface(image);

	glActiveTexture(GL_TEXTURE3); // Holding tangent information
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &texid[3]);
	glBindTexture(GL_TEXTURE_3D, texid[3]);
}


void InitVBOs(GLuint vboid[])
{
    GLfloat vertices[] = {
		-1.0,-1.0, 1.0,   1.0,-1.0, 1.0,   1.0, 1.0, 1.0,  -1.0, 1.0, 1.0, // Front
		-1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,   1.0, 1.0,-1.0,   1.0,-1.0,-1.0, // Back
		-1.0, 1.0,-1.0,  -1.0, 1.0, 1.0,   1.0, 1.0, 1.0,   1.0, 1.0,-1.0, // Top
		-1.0,-1.0,-1.0,   1.0,-1.0,-1.0,   1.0,-1.0, 1.0,  -1.0,-1.0, 1.0, // Bottom
		 1.0,-1.0,-1.0,   1.0, 1.0,-1.0,   1.0, 1.0, 1.0,   1.0,-1.0, 1.0, // Right
		-1.0,-1.0,-1.0,  -1.0,-1.0, 1.0,  -1.0, 1.0, 1.0,  -1.0, 1.0,-1.0  // Left
	};

	GLfloat texcoords[] = {
		1.0, 1.0,  0.0, 1.0,  0.0, 0.0,  1.0, 0.0, // Front
		0.0, 1.0,  0.0, 0.0,  1.0, 0.0,  1.0, 1.0, // Back
		1.0, 0.0,  1.0, 1.0,  0.0, 1.0,  0.0, 0.0, // Top
		0.0, 0.0,  1.0, 0.0,  1.0, 1.0,  0.0, 1.0, // Bottom
		0.0, 1.0,  0.0, 0.0,  1.0, 0.0,  1.0, 1.0, // Right
		1.0, 1.0,  0.0, 1.0,  0.0, 0.0,  1.0, 0.0  // Left
	};


	GLfloat normals[] = {
		 0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f, // Front
		 0.0f, 0.0f,-1.0f,   0.0f, 0.0f,-1.0f,   0.0f, 0.0f,-1.0f,	 0.0f, 0.0f,-1.0f, // Back
		 0.0f, 1.0f, 0.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1.0f, 0.0f,   0.0f, 1.0f, 0.0f, // Top
		 0.0f,-1.0f, 0.0f,   0.0f,-1.0f, 0.0f,   0.0f,-1.0f, 0.0f,   0.0f,-1.0f, 0.0f, // Bottom
		 1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f, // Right
		-1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f  // Left
	};

	GLfloat tangents[] = {
		-1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f, // Front
		 1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f, // Back
		-1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f,  -1.0f, 0.0f, 0.0f, // Top
		 1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f,   1.0f, 0.0f, 0.0f, // Bottom
		 0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f,   0.0f, 0.0f, 1.0f, // Right
		 0.0f, 0.0f,-1.0f,   0.0f, 0.0f,-1.0f,   0.0f, 0.0f,-1.0f,   0.0f, 0.0f,-1.0f  // Left
	};
	
	glGenBuffers(4, vboid);

	glBindBuffer(GL_ARRAY_BUFFER, vboid[0]);
	glBufferData(GL_ARRAY_BUFFER, (24*3)*sizeof(GLfloat), vertices, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vboid[1]);
	glBufferData(GL_ARRAY_BUFFER, (24*2)*sizeof(GLfloat), texcoords, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vboid[2]);
	glBufferData(GL_ARRAY_BUFFER, (24*3)*sizeof(GLfloat), normals, GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, vboid[3]);
	glBufferData(GL_ARRAY_BUFFER, (24*3)*sizeof(GLfloat), tangents, GL_STATIC_DRAW);
}
