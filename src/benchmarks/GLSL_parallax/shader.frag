// FRAGMENT: Phong shading with Blinn-Phong lighting, Point light (per pixel)
// Tangent space parallax mapping with offset limiting

uniform sampler2D Tex0, Tex1, Tex2;
varying vec3 ecPos, normal, lightDir;
varying vec4 ambientGlobal, ambient, diffuse;

void main (void)
{
	vec3 nor, halfV;
	float NdotL, NdotHV;
	float dist, att;
	vec2 TexCoord;
	float height;
	float scale = 0.04;
	float bias = 0.02;

	ecPos = normalize(ecPos);

	// alpha channel encodes the height map
	height = scale * texture2D(Tex1, gl_TexCoord[1].st).a - bias;
	TexCoord = gl_TexCoord[0].st + height * ecPos.xy;

	vec4 colorMap = texture2D(Tex0, TexCoord);
	vec4 normalMap = texture2D(Tex1, TexCoord);
	vec4 glossMap = texture2D(Tex2, TexCoord);
	nor = 2.0 * normalMap.rgb - 1.0; // decoding normal map

    dist = length(lightDir);
	lightDir = normalize(lightDir);

	att = 1.0 / (gl_LightSource[0].constantAttenuation +
                 gl_LightSource[0].linearAttenuation * dist +
                 gl_LightSource[0].quadraticAttenuation * dist * dist);

	gl_FragColor = ambientGlobal + ambient * att;

	NdotL = max(dot(nor, lightDir), 0.0);

	if (NdotL > 0.0) {
		gl_FragColor += att * (diffuse * NdotL);
		halfV = normalize(lightDir + ecPos);
		NdotHV = max(dot(nor, halfV), 0.0);
		vec4 spec = att * gl_FrontMaterial.specular * gl_LightSource[0].specular * pow(NdotHV, gl_FrontMaterial.shininess);
		spec *= glossMap; // specular mapping
		gl_FragColor += spec;
	}

	gl_FragColor *= colorMap;
}
