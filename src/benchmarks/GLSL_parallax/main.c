/* GLSL_parallax: Parallax mapping with GLSL shaders
   Copyright (C) 2007 Angelo "Encelo" Theodorou
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glext.h>

#include "init.h"
#include "extra.h"
#include "../get_options.h"
#include "../check_time.h"


int main(int argc, char **argv)
{
	options opt;
	SDL_Surface *screen;
	SDL_Event event;
	const SDL_VideoInfo* info = NULL;
	int bpp = 0;
	int flags = 0;

	const GLubyte* string;
	GLint value;

	struct Material m = {
		{0.6, 0.6, 0.6, 1.0},
		{0.3, 0.3, 0.3, 1.0},
		{100.0}
	};
	struct Light l = {
		{0.2, 0.2, 0.2, 1.0},
		{1.0, 1.0, 1.0, 1.0},
		{1.0, 1.0, 1.0, 1.0},
		{0.0, 0.75, 1.5, 1.0},
	};
	struct Shader sh;
	GLuint texid[4];
	GLuint vboid[4];

	int quit = 0;
	Uint32 interval;
	GLfloat angleLight = 0.0f;
	GLfloat angleCube = 0.0f;


/* ----- Gathering options -------- */
	get_options(argc, argv, &opt);


/* ----- SDL init --------------- */
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Video initialization failed: %s\n", SDL_GetError());
		exit(-1);
	}

	atexit(SDL_Quit);
	
	info = SDL_GetVideoInfo();
	bpp = info->vfmt->BitsPerPixel;

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);
	
	if (opt.fullscreen == OPT_TRUE)
		flags = SDL_OPENGL | SDL_FULLSCREEN;
	else
		flags = SDL_OPENGL;

	
/* ----- Setting up the screen surface --------------- */
	if((screen = SDL_SetVideoMode(opt.width, opt.height, bpp, flags)) == 0) {
		fprintf(stderr, "Video mode set failed: %s\n", SDL_GetError());
        	exit(-1);
	}

	SDL_WM_SetCaption("GLSL_parallax by Encelo", NULL);


/* ----- Checking for OpenGL 2 --------------- */
	string = glGetString(GL_VERSION);
	if (string[0] != '2') {
		printf("OpenGL 2 not supported!\n");
		exit(-1);
	}


/* ----- OpenGL init --------------- */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/* The following function replaces gluPerspective */
	perspectiveGL(45.0f, (GLfloat)opt.width/(GLfloat)opt.height, 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);
	glEnable(GL_DEPTH_TEST);

	InitLighting(&l, &m);
	InitShaders(&sh);
	InitTextures(texid);
	InitVBOs(vboid);

	glActiveTexture(GL_TEXTURE1);
	glDisable(GL_TEXTURE_2D);
	glActiveTexture(GL_TEXTURE2);
	glDisable(GL_TEXTURE_2D);


/* ----- Event cycle --------------- */
	while (!quit) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				quit = 1;
				break;
			case SDL_KEYDOWN:
				switch(event.key.keysym.sym) {
				case SDLK_ESCAPE:
				case SDLK_q:
					quit = 1;
					break;
				}
				break;
			}
		}

		/* Exit after 'opt.time' seconds */
		if(check_time(opt.time))
			break;

		interval = FrameTiming();

		
/* ----- Blitting on the screen --------------- */
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		angleCube += 0.1f * interval;
		angleLight += 0.001 * interval;

		l.position[0] = sinf(angleLight) * 1.0f;
		l.position[1] = sinf(angleLight) * 1.0f;
		glLoadIdentity();
		glTranslatef(0.0f, 0.0f, -6.0f);
		glRotatef(15.0f, 1.0f, 0.0f, 0.0f);

		glUseProgram(NULL);
		UpdateLight(&l);
		glUseProgram(sh.p);

		glRotatef(angleCube, 0.0f, 1.0f, 0.0f);

		DrawCube(vboid);

		SDL_GL_SwapBuffers();		
		/*SDL_Delay(10); */  /* Decomment this if you want 1/50th screen update */
	}

	
/* ----- Quitting --------------- */
	glDeleteBuffers(4, vboid);
	glDeleteTextures(4, texid);

	glDetachShader(sh.p, sh.v);
	glDetachShader(sh.p, sh.f);
	glDeleteShader(sh.v);
	glDeleteShader(sh.f);
	glDeleteProgram(sh.p);

	SDL_Quit();
	return 0;	
}
