// VERTEX: Phong shading with Blinn-Phong lighting, Point light (per pixel)
// Tangent space parallax mapping with offset limiting

varying vec3 ecPos, normal, lightDir;
varying vec4 ambientGlobal, ambient, diffuse;

void main(void)
{
	ecPos = vec3(gl_ModelViewMatrix * gl_Vertex);
	normal = normalize(gl_NormalMatrix * gl_Normal);
	vec3 tangent = normalize(gl_NormalMatrix * vec3(gl_MultiTexCoord3));
	vec3 binormal = cross(normal, tangent);
	mat3 tbnMatrix = mat3(tangent, binormal, normal);

	lightDir = gl_LightSource[0].position.xyz - ecPos;
	lightDir = tbnMatrix * lightDir;
	ecPos = tbnMatrix * (-ecPos);
	
	ambientGlobal = gl_LightModel.ambient * gl_FrontMaterial.ambient;
    ambient = gl_LightSource[0].ambient * gl_FrontMaterial.ambient;
	diffuse = gl_FrontMaterial.diffuse * gl_LightSource[0].diffuse;

	normal = tbnMatrix * normal;
	gl_Position = ftransform();

	gl_TexCoord[0] = gl_MultiTexCoord0;
	gl_TexCoord[1] = gl_MultiTexCoord1;
	gl_TexCoord[2] = gl_MultiTexCoord2;
}
