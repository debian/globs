/* GLSL_parallax: Parallax mapping with GLSL shaders
   Copyright (C) 2007 Angelo "Encelo" Theodorou
*/


struct Shader {
	GLuint v, f, p;
	GLint tex0, tex1, tex2;
};

struct Light {
    GLfloat ambient[4];
	GLfloat diffuse[4];
	GLfloat specular[4];
	GLfloat position[4];
};

struct Material {
	GLfloat mat_diffuse[4];
	GLfloat mat_specular[4];
	GLfloat mat_shininess[1];
};

void InitLighting(struct Light *l, struct Material *m);
void InitShaders(struct Shader *sh);
void InitTextures(GLuint texid[]);
void InitVBOs(GLuint vboid[]);
