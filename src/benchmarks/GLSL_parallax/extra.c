/* GLSL_parallax: Parallax mapping with with GLSL shaders
   Copyright (C) 2007 Angelo "Encelo" Theodorou
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <SDL/SDL.h>
#include <GL/gl.h>
#include <GL/glext.h> 

#include "init.h"

void UpdateLight(struct Light *l)
{
	glLightfv(GL_LIGHT0, GL_POSITION, l->position);
	glLightfv(GL_LIGHT0, GL_AMBIENT, l->ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, l->diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, l->specular);

	glPushAttrib(GL_ENABLE_BIT);
	glDisable(GL_TEXTURE_2D);
	glDisable(GL_LIGHTING);

	glColor3f(l->diffuse[0], l->diffuse[1], l->diffuse[2]);
	glPushMatrix();
	glBegin(GL_QUADS);
		glVertex3f(l->position[0]+0.025f, l->position[1]+0.025f, l->position[2]);
		glVertex3f(l->position[0]-0.025f, l->position[1]+0.025f, l->position[2]);
		glVertex3f(l->position[0]-0.025f, l->position[1]-0.025f, l->position[2]);
		glVertex3f(l->position[0]+0.025f, l->position[1]-0.025f, l->position[2]);
	glEnd();
	glPopMatrix();

	glPopAttrib();
}


void DrawCube(GLuint vboid[])
{
	glEnableClientState(GL_VERTEX_ARRAY);
	glBindBuffer(GL_ARRAY_BUFFER, vboid[0]);
	glVertexPointer(3, GL_FLOAT, 0, NULL);

	glClientActiveTexture(GL_TEXTURE0);
	glBindBuffer(GL_ARRAY_BUFFER, vboid[1]);
	glTexCoordPointer(2, GL_FLOAT, 0, NULL);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glClientActiveTexture(GL_TEXTURE1);
	glBindBuffer(GL_ARRAY_BUFFER, vboid[1]);
	glTexCoordPointer(2, GL_FLOAT, 0, NULL);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glClientActiveTexture(GL_TEXTURE2);
	glBindBuffer(GL_ARRAY_BUFFER, vboid[1]);
	glTexCoordPointer(2, GL_FLOAT, 0, NULL);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glBindBuffer(GL_ARRAY_BUFFER, vboid[2]);
	glNormalPointer(GL_FLOAT, 0, NULL);
	glEnableClientState(GL_NORMAL_ARRAY);

	glClientActiveTexture(GL_TEXTURE3);
	glBindBuffer(GL_ARRAY_BUFFER, vboid[3]);
	glTexCoordPointer(3, GL_FLOAT, 0, NULL);
	glEnableClientState(GL_TEXTURE_COORD_ARRAY);

	glDrawArrays(GL_QUADS, 0, 24);
}


/* Replaces gluPerspective. Sets the frustum to perspective mode.
 * fovY     - Field of vision in degrees in the y direction
 * aspect   - Aspect ratio of the viewport
 * zNear    - The near clipping distance
 * zFar     - The far clipping distance
*/
void perspectiveGL(GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar)
{
	GLdouble fW, fH;

	fH = tan( (fovY / 2) / 180 * M_PIl ) * zNear;
	fW = fH * aspect;

	glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}


/* Calculate frame interval and print FPS each 5s */
int FrameTiming(void)
{
	Uint32 interval;
	static Uint32 current, last = 0, five = 0, nframes = 0;

	current = SDL_GetTicks();
	nframes++;

	if (current - five > 5*1000) {
		printf("%u frames in 5 seconds = %.1f FPS\n", nframes, (float)nframes/5.0f);
		nframes = 0;
		five = current;
	}
	
	interval = current - last;
	last = current;
	
	return interval;
}


/* Print errors from shaders and programs */
void printObjectInfoLog(GLuint obj)
{
	int infologLength = 0;
	int charsWritten  = 0;
	char *infoLog;

	if (glIsShader(obj) == GL_TRUE)
		glGetShaderiv(obj, GL_INFO_LOG_LENGTH, &infologLength);
	else
		glGetProgramiv(obj, GL_INFO_LOG_LENGTH, &infologLength);

	if (infologLength > 1) {
		infoLog = (char *)malloc(infologLength);
		if (glIsShader(obj) == GL_TRUE) {
			glGetShaderInfoLog(obj, infologLength, &charsWritten, infoLog);
			printf("Shader InfoLog: %s\n", infoLog);
		}
		else {
			glGetProgramInfoLog(obj, infologLength, &charsWritten, infoLog);
			printf("Program InfoLog: %s\n", infoLog);
		}
		free(infoLog);
	}
}
