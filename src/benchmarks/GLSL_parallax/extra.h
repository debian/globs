/* GLSL_parallax: Parallax mapping with GLSL shaders
   Copyright (C) 2007 Angelo "Encelo" Theodorou
*/


void UpdateLight(struct Light *l);
void DrawCube(GLuint vboid[]);
void perspectiveGL(GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar);
int FrameTiming(void);
GLboolean CheckExtension(char *extName);
void printObjectInfoLog(GLuint obj);
