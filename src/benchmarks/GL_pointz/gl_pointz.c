/* GL_pointz: a simple demo showing a waving cube made up of points
   Copyright (C) 2005 Angelo "Encelo" Theodorou
 
   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation; either version 2 of the License, or
   (at your option) any later version.
 
   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.
  
   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software
   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/


#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <SDL/SDL.h>
#include <GL/gl.h>

#include "../get_options.h"
#include "../check_time.h"

#ifndef M_PI
    #define M_PI 3.14159265358979323846
#endif

#ifndef M_PIl
	# define M_PIl 3.1415926535897932384626433832795029L
#endif

#define NPOINTS 50 /* Grid density */
#define DISTANCE 0.1f /* Space between points */
#define POINTSIZE 1.0f
#define NPLANES 10 /* Number of planes */
#define SIN_ANGLE 0.3f /* Sin waving speed */
#define ROTY 0.05f /* Y axis rotation speed */

int FrameTiming(void);
void perspectiveGL(GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar);
GLfloat clamp(GLfloat value, GLfloat min, GLfloat max);


int main(int argc, char **argv)
{
	options opt;
	
	SDL_Surface *screen;
	SDL_Event event;
	int quit = 0;
	
	const SDL_VideoInfo* info = NULL;
	int bpp = 0;
	int flags = 0;

	Uint32 interval;
	GLuint Plane;
	GLfloat angle = 0.0f;
	GLfloat roty = 0.0f;
	GLfloat points[NPOINTS][NPOINTS][3];


/* ----- Gathering options -------- */
	get_options(argc, argv, &opt);


/* ----- SDL init --------------- */
	if(SDL_Init(SDL_INIT_VIDEO) < 0) {
		fprintf(stderr, "Video initialization failed: %s\n", SDL_GetError());
		exit(-1);
	}

	atexit(SDL_Quit);
	
	info = SDL_GetVideoInfo();
	bpp = info->vfmt->BitsPerPixel;

	
/* ----- OpenGL attribute setting via SDL --------------- */
	SDL_GL_SetAttribute(SDL_GL_RED_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_GREEN_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_BLUE_SIZE, 5);
	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);

	if (opt.fullscreen == OPT_TRUE)
		flags = SDL_OPENGL | SDL_FULLSCREEN;
	else
		flags = SDL_OPENGL;


/* ----- Setting up the screen surface --------------- */
	if((screen = SDL_SetVideoMode(opt.width, opt.height, bpp, flags)) == 0) {
		fprintf(stderr, "Video mode set failed: %s\n", SDL_GetError());
        	exit(-1);
	}

	SDL_WM_SetCaption("GL_pointz by Encelo", NULL);


/* ----- OpenGL init --------------- */
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	/* The following function replaces gluPerspective */
	perspectiveGL(45.0f, (GLfloat)opt.width/(GLfloat)opt.height, 0.1f, 100.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	glEnable(GL_POINT_SMOOTH);
	glHint(GL_POINT_SMOOTH_HINT, GL_NICEST);
	glPointSize(POINTSIZE);


/* ----- Plane init --------------- */
	int i, j;
	for(i=0; i<NPOINTS; i++) {
		for(j=0; j<NPOINTS; j++) {
			points[i][j][0] = NPOINTS*(-DISTANCE/2.0f) + i*DISTANCE;
			points[i][j][1] = 0.0f;
			points[i][j][2] = NPOINTS*(DISTANCE/2.0f) - j*DISTANCE;
		}
	}

	Plane = glGenLists (1);
	
	
/* ----- Event cycle --------------- */
	while (!quit) {
		while (SDL_PollEvent(&event)) {
			switch (event.type) {
			case SDL_QUIT:
				quit = 1;
				break;
			case SDL_KEYDOWN:
				switch( event.key.keysym.sym ){
				case SDLK_ESCAPE:
				case SDLK_q:
					quit = 1;
					break;
				case SDLK_F1:
					SDL_WM_ToggleFullScreen(screen);
					break;
				}
			}
		}

		/* Exit after 'opt.time' seconds */
		if(check_time(opt.time))
			break;

		interval = FrameTiming();

		
/* ----- Blitting on the screen --------------- */
		glClear(GL_COLOR_BUFFER_BIT);

		int i, j, k;
		angle += SIN_ANGLE * interval;
		roty += ROTY * interval;
		
		glNewList(Plane, GL_COMPILE);
		glBegin(GL_POINTS);
			for(i=0; i<NPOINTS; i++)
				for(j=0; j<NPOINTS; j++) {
					glColor3f(clamp(points[i][j][0], 0.0, 1.0), 
						clamp(points[i][j][1], 0.0, 1.0), 
						clamp(points[i][j][2], 0.0, 1.0));
					glVertex3f(points[i][j][0],
						points[i][j][1] + sin((M_PI/180 * angle) + points[i][j][0] + points[i][j][2]) * 0.25f,
						points[i][j][2]);
				}
		glEnd();
		glEndList();

		
		glLoadIdentity();
		glTranslatef(0.0f, 2.75f, -9.0f);
		glRotatef(30.0f, 1.0f, 0.0f, 0.0f);
		glRotatef(roty, 0.0f, 1.0f, 0.0f);

		for (k=0; k<NPLANES; k++) {
			glTranslatef(0.0f, -(NPOINTS*DISTANCE)/NPLANES, 0.0f);
			glCallList(Plane);
		}
		

		SDL_GL_SwapBuffers();		
/*		SDL_Delay(20); */ /* Decomment this if you want 1/50th screen update */
	}

	
/* ----- Quitting --------------- */
	glDeleteLists(Plane, 1);
	SDL_Quit();
	return 0;	
}


/* Calculate frame interval and print FPS each 5s */
int FrameTiming(void)
{
	Uint32 interval;
	static Uint32 current, last = 0, five = 0, nframes = 0;

	current = SDL_GetTicks();
	nframes++;

/*
	if (current - five > 5*1000) {
		printf("%u frames in 5 seconds = %.1f FPS\n", nframes, (float)nframes/5.0f);
		nframes = 0;
		five = current;
	}
*/
	
	interval = current - last;
	last = current;
	
	return interval;
}


// Replaces gluPerspective. Sets the frustum to perspective mode.
// fovY     - Field of vision in degrees in the y direction
// aspect   - Aspect ratio of the viewport
// zNear    - The near clipping distance
// zFar     - The far clipping distance
void perspectiveGL(GLdouble fovY, GLdouble aspect, GLdouble zNear, GLdouble zFar)
{
	GLdouble fW, fH;

	fH = tan( (fovY / 2) / 180 * M_PIl ) * zNear;
	fW = fH * aspect;

	glFrustum( -fW, fW, -fH, fH, zNear, zFar );
}


/* Clamp a value that is not between min and max */
GLfloat clamp(GLfloat value, GLfloat min, GLfloat max)
{
	if (value > max)
		return max;
	else if (value < min)
		return min;
	else
		return value;
}
