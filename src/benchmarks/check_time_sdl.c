#include <SDL.h>

long int check_time(int time)
{
	static Uint32 current, first = 0;
	static long int nframes = 0;

	/* first initialization on check_time first run */
	if(first == 0)
		first = SDL_GetTicks();

	current = SDL_GetTicks();
	nframes++;

	if (current - first > time*1000) {
		printf("fps = %.1f\n", (float)nframes/(float)time);
		return nframes;
	}
	else
		return 0l;
}
