#include <stdio.h>
#include <sys/time.h>

long int check_time(int time)
{
	static struct timeval current, first, interval;
	static long int nframes = 0;
	static int first_time = 0;

	/* first initialization on check_time first run */
	if(first_time == 0) {
		gettimeofday(&first, NULL);
		first_time = 1;
	}

	gettimeofday(&current, NULL);
	nframes++;

	timersub(&current, &first, &interval);
	
	if (interval.tv_sec >= time) {
		printf("fps = %.1f\n", (float)nframes/(float)time);
		return nframes;
	}
	else
		return 0l;
}
