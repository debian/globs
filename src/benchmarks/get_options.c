#include <getopt.h>
#include "get_options.h"

error_cond get_options(int argc, char **argv, options *opt)
{
	extern char *optarg;
	extern int optind, optopt, opterr;
	int option_index = 0;
	static struct option long_options[] = {
		{"fullscreen", 0, 0, 'f'},
		{"width", 1, 0, 'w'},
		{"height", 1, 0, 'h'},
		{"time", 1, 0, 't'},
		{0, 0, 0, 0}
	};
	int c, ret;
	
	/* Setting defaults */
	opt->width = DEFAULT_WIDTH;
	opt->height = DEFAULT_HEIGHT;
	opt->time = DEFAULT_TIME;
	
	ret = NO_ERR;
	opt->fullscreen = OPT_FALSE;
	
	while ((c = getopt_long(argc, argv, ":fw:h:t:", long_options, &option_index)) != -1) {
//	while ((c = getopt(argc, argv, ":fw:h:t:")) != -1) {
		switch(c) {
		case 'f':
			opt->fullscreen = OPT_TRUE;
			break;
		case 'w':
			if(atoi(optarg))
				opt->width = atoi(optarg);
			break;
		case 'h':
			if(atoi(optarg))
				opt->height = atoi(optarg);
			break;
		case 't':
			if(atoi(optarg))
				opt->time = atoi(optarg);
			break;
		case ':':
			ret = NOVALUE_ERR;
			break;
		case '?':
			ret = UNKNOWN_ERR;
			break;
		}
	}

	return ret;
}
