/* GL_smoke: a simple particle engine demo showing white smoke
 * Copyright (C) 2005 Angelo "Encelo" Theodorou
 */

#include <stdlib.h>
#include <SDL/SDL.h>
#include <GL/gl.h>

#include "particle.h"


particle* particles_alloc(int num, char *tex_name, GLuint *tex_num)
{
	particle *particles;
	SDL_Surface *tex_img;
	glGenTextures(1, tex_num);
	
	particles = (particle *)calloc(num, sizeof(particle));

	if(tex_img = (SDL_Surface *) IMG_Load(tex_name)) {
		glBindTexture(GL_TEXTURE_2D, *tex_num);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, tex_img->w, tex_img->h, 0, GL_RGBA, GL_UNSIGNED_BYTE, tex_img->pixels);
		SDL_FreeSurface (tex_img);
	}
	else
		printf("Texture not found! Using gray quads.\n");
	
	return particles;
}


void particles_init(particle* particle, int num)
{
	int i;

	for(i=0; i < num; i++) {
		particle[i].active = TRUE;
		particle[i].life = 1.0f; /* Full life*/
		particle[i].fade = ((float)(rand()%100))/25000.0f+0.000077f; /* Random fade speed */
		particle[i].size = ((float)(rand()%40))/100.0f+0.6f; /* Random size variation */
		particle[i].r = particle[i].g = particle[i].b = ((float)(rand()%20))/100 + 0.1f; /* Shade of gray */
		particle[i].x = particle[i].y = particle[i].z = 0.0f; /* Center of the scene */
		/* Random speeds */
		particle[i].xi = ((float)(rand()%10)-5.0f)*3.5f; /* Small dispersion */
		particle[i].yi = ((float)(rand()%10)); /* Nearly no dispersion*/
		particle[i].zi = ((float)(rand()%10)-5.0f)*3.5f; /* Small dispersion */
		/* Gravity */
		particle[i].xg = 0.0f;
		particle[i].yg = 0.004f; /* A slow ascension */
		particle[i].zg = 0.0f;
	}
}


void particles_setpos(particle* particle, int num, float x, float y, float z)
{
	int i;
	
	for(i=0; i < num; i++) {
		 particle[i].x = x;
		 particle[i].y = y;
		 particle[i].z = z;
	}
}


void particles_simul(particle* particle, int num, int interval)
{
	int i;

	for(i=0; i < num; i++) {
		/* Position change based on direction speed */
		particle[i].x += ((particle[i].xi * (1.0f-particle[i].life))/25000) * interval; /* Greater dispersion for older particles */
		particle[i].y += (particle[i].yi/25000) * interval;
		particle[i].z += ((particle[i].zi * (1.0f-particle[i].life))/25000) * interval;  /* Greater dispersion for older particles */

		/* Speed change based on gravity */
		particle[i].xi += particle[i].xg * interval;
		particle[i].yi += particle[i].yg * interval;
		particle[i].zi += particle[i].zg * interval;

		/* Life reduction based on fade */
		particle[i].life -= particle[i].fade * interval;

		if(particle[i].life < 0)
			particles_init(&particle[i], 1);
	}
}


void particles_draw(particle* particle, int num)
{
	int i;
	float size;
	float x, y, z;

	for(i=0; i < num; i++) {
		if(particle[i].active == FALSE)
			continue;
		
		x = particle[i].x;
		y = particle[i].y;
		z = particle[i].z;
		size = particle[i].size;

		glColor4f(particle[i].r, particle[i].g, particle[i].b, particle[i].life);

		glBegin(GL_TRIANGLE_STRIP);
			glTexCoord2d(1, 1); glVertex3f(x+(0.5f*size), y+(0.5f*size), z);
			glTexCoord2d(0, 1); glVertex3f(x-(0.5f*size), y+(0.5f*size), z);
			glTexCoord2d(1, 0); glVertex3f(x+(0.5f*size), y-(0.5f*size), z);
			glTexCoord2d(0, 0); glVertex3f(x-(0.5f*size), y-(0.5f*size), z);
		glEnd();
	}
}


void particles_dealloc(particle* particles, GLuint tex_num)
{
	glDeleteTextures(1, &tex_num);
	free(particles);
}

