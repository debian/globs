/* GL_smoke: a simple particle engine demo showing white smoke
 * Copyright (C) 2005 Angelo "Encelo" Theodorou
 */

#define TRUE 1
#define FALSE 0

typedef struct {
	int active;        /* Should the particle state be updated? */
	float life;        /* Remaining life 0.0 - 1.0 */
	float fade;        /* How much life decrease every simulation cycle */
	float size;        /* Size scaling */
	float r, g, b;     /* Color 0.0 - 1.0 */
	float x, y, z;     /* Position */
	float xi, yi, zi;  /* Speed */
	float xg, yg, zg;  /* Gravity */
} particle;

particle* particles_alloc(int num, char* tex_name, GLuint *tex_num);
void particles_init(particle* particle, int num);
void particles_setpos(particle* particle, int num, float x, float y, float z);
void particles_simul(particle* particle, int num, int interval);
void particles_draw(particle* particle, int num);
void particles_dealloc(particle* particles, GLuint tex_num);
