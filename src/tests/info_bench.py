#!/usr/bin/env python
## info_bench.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

from Globs import benchmarks

bm = benchmarks.Benchmarks('../benchmarks')

for name in bm.get_names():
	#info = bm.get_info(name)
	info = bm[name] # Using special method __getitem__()
	print 'Name: ' + info['name']
	print 'Author: ' + info['author']
	print 'Version: ' + info['version']
	print 'Description: ' + info['description']

	if info.has_key('extensions'):
		print 'Extensions: ' + str(info['extensions'])
	
	print
