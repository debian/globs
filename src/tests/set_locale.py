locale_dir = '../../po'

import locale
import gettext

def set():
	locale.setlocale(locale.LC_ALL, '')
	gettext.bindtextdomain('globs', locale_dir)
	gettext.textdomain('globs')
	gettext.install('globs', locale_dir, unicode=1)
