#!/usr/bin/env python

import xml.parsers.expat

xmlfile = '../../globs.glade'

def start_el(name, attrs):
	#print 'Start: ', name, attrs
	pass
def end_el(name):
	#print 'End: ', name
	pass
def char_data(data):
	#print 'Character data:', repr(data)
	pass

p = xml.parsers.expat.ParserCreate()

p.StartElementHandler = start_el
p.EndElementHandler = end_el
p.CharacterDataHandler = char_data

string = ''
f = open(xmlfile, 'r')
for line in f.readlines():
	string += line
f.close()

p.Parse(string)
