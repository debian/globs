#!/usr/bin/env python
## submit_test.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import set_locale
set_locale.set()
from Globs import local_db, benchmarks, user, hwd, submit

bench_name = 'Fake'
db = local_db.Local_Database('test.db')
bm = benchmarks.Benchmarks('../benchmarks')
user = user.User('test.db')
hwd = hwd.HWDetect()
submit = submit.Submit(db, user, hwd, 'http://globs.sourceforge.net/db_submit.php')

db.clear()
info = bm.get_info(bench_name)
opts = info['defaults']
fps = bm.run(bench_name, None)
date = db.insert(info, opts, fps)

print 'Bench name: ' + bench_name
print 'FPS: ' + str(fps)
print 'Date: ' + date
print

data = {'user':'encelo', 'password':'pass', 'name': 'Angelo Theodorou', 'location':'Naples, Italy', 'email':'encelo@users.sourceforge.net', 'homesite':'http://autistici.org/encelo'}

user.update(data)
submit.send(date)

data['password'] = 'wrong'

print
user.update(data)
submit.send(date)
