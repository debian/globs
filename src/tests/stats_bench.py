#!/usr/bin/env python
## stats_bench.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import time as t
from Globs import statistics

try:
	import matplotlib.numerix as n
except ImportError:
	with_matplotlib = False # Matplotlib and NumPy are not installed!
else:
	with_matplotlib = True


name = 'Test'
implementations = []
st_ar = statistics.Statistics_Array([name])
implementations.append(st_ar)
if with_matplotlib == True:
	st_ix = statistics.Statistics_Numerix([name])
	implementations.append(st_ix)

n = 10000 # Number of insertion in the array
print 'N. insertion: ' + str(n)
print
for st in implementations:
	if str(st).find('Numerix') >= 0:
		print 'Numerix version'
	else:
		print 'Built-in Array module version'
	start = t.time()
	for i in range(n):
		st.append(i, name)
	stop_1 = t.time()

	max = st.get_max(name)
	min = st.get_min(name)
	last = st.get_last(name)
	stop_2 = t.time()
	avg = st.get_avg(name)
	stop_3 = t.time()

	print 'Last 5 elements: %s' % str(st.get_array(name, 5))
	print 'Min: %f, Avg: %f, Max: %f, Last: %f' % (min, avg, max, last)
	print 'Insertion time: %f' % (stop_1-start)
	print 'Get Max, Min, Last time: %f' % (stop_2-stop_1)
	print 'Get Avg time: %f' % (stop_3-stop_2)
	print 'Total time: %f' % (stop_3-start)
	print
