#!/usr/bin/env python
## run_bench.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import set_locale
set_locale.set()
from Globs import benchmarks, statistics

bm = benchmarks.Benchmarks('../benchmarks')
st = statistics.Statistics(bm.get_names())
opts = None
#opts = {"fullscreen":True, "width":1024, "height":768, "time":5}

for name in bm.get_names():
	print 'Benchmark: ' + name
	fps = bm.run(name, opts)
	st.append(fps, name)
	if (fps != None):
		print 'Options dictionary: ' + str(bm.get_options(name))
		print 'FPS: ' + str(fps)
	print

count = 0
total = 0
print 'Fake benchmark ignored'
for name in bm.get_names():
	if name != 'Fake':
		max = st.get_max(name)
		if max != None:
			count += 1
			total += max
		else:
			continue
	
print 'N. benchmarks executed: ' + str(count)
print 'Mean FPS = ' + str(total/count)
