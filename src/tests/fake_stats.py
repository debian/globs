#!/usr/bin/env python
## fake_stats.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

from Globs import benchmarks, statistics

name = 'Fake'
bm = benchmarks.Benchmarks('../benchmarks')
st = statistics.Statistics([name])

opts = {"fullscreen":False, "width":640, "height":480, "time":2}
n = 5

print 'Benchmark: ' + name
print 'Options dictionary: ' + str(opts)
print
for i in range(n):
	fps = bm.run(name, opts)
	st.append(fps, name)
	if (fps != None):
		print 'FPS: ' + str(fps)

max = st.get_max(name)
min = st.get_min(name)
last = st.get_last(name)
avg = st.get_avg(name)
print
print 'N. executions: ' + str(n)
print 'Min: %f, Avg: %f, Max: %f, Last: %f' % (min, avg, max, last)
