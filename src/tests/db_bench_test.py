#!/usr/bin/env python
## db_bench_test.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import set_locale
set_locale.set()
from Globs import local_db, benchmarks

db = local_db.Local_Database('test.db')
bm = benchmarks.Benchmarks('../benchmarks')

for name in bm.get_names():
	info = bm.get_info(name)
	opts = info['defaults']
	fps = bm.run(name)
	if(db.insert(info, opts, fps)):
		print name + ' inserted into the db'

print
print 'The entire table:'
db.show()
print
print 'GL_shadow deleted:'
db.delete('GL_shadow')
db.show()
print
print 'All deleted except Fake and GL_smoke:'
db.purge(('Fake', 'GL_smoke'))
db.show()
print
print 'Table cleared.'
db.clear()
db.show()
