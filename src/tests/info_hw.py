#!/usr/bin/env python
## info_hw.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

from Globs import hwd

hwd = hwd.HWDetect()

cpu_list = hwd['cpus']
mem = hwd['mem']
gl = hwd['gl']

print
print 'Hostname:', hwd['host']
print
for cpu in cpu_list:
	print 'CPU' + str(cpu_list.index(cpu))
	print 'Model:', cpu['model']
	print 'Frequency:', cpu['frequency'], 'MHz'
	print 'BogoMIPS:', cpu['bogomips']
print
print 'MEM'
print 'Physical:', mem['physical'], 'kB'
print 'Swap:', mem['swap'], 'kB'
print
print 'GL'
print 'Vendor:', gl['vendor']
print 'Renderer:', gl['renderer']
print 'Version:', gl['version']
print 'N. Extensions:', len(gl['extensions'])
print
