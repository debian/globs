#!/usr/bin/env python
## submit_test.py
##
## GL O.B.S.: GL Open Benchmark Suite
## Copyright (C) 2006-2007 Angelo Theodorou <encelo@users.sourceforge.net>
##
## This program is free software; you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation; either version 2 of the License, or
## (at your option) any later version.
##
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with this program; if not, write to the Free Software
## Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
##

import set_locale
set_locale.set()
from Globs import user

def user_print(user):
	"""Print user information stored inside an User clas"""
	
	print 'User: ' + str(user['user'])
	print 'Password: ' + str(user['password'])
	print 'Name: ' + str(user['name'])
	print 'Location: ' + str(user['location'])
	print 'e-mail: ' + str(user['email'])
	print 'Homesite: ' + str(user['homesite'])


user = user.User('test.db')
print
user_data = {'user': 'encelo', 'password': 'mypass', 'name': 'Angelo Theodorou', 'location':  'Naples, Italy', 'email': 'encelo@users.sourceforge.net', 'homesite': 'http://autistici.org/encelo'}
user.update(user_data)
user_print(user)
print
user_data = {'user': 'encelo', 'password': 'different_pass', 'name': 'Angelo "Encelo" Theodorou', 'location':  'Naples, Italy', 'email': 'encelo@gmail.com', 'homesite': 'http://autistici.org/encelo'}
user.update(user_data)
user_print(user)
print
