globs: GL Open Benchmark Suite
Copyright (C) 2006-2007 Angelo "Encelo" Theodorou

http://globs.sourceforge.net

Info
----
GL O.B.S. is a framework for OpenGL benchmarks.
It's based upon a program that interacts with the user through a gui and a series of 
benchmarks which share a common options set and shell output format.

Dependencies
------------
SCons (Building)
Python >= 2.6 (Program)
GTK2 >= 2.16, PyGTK, Libglade (GUI)
SQLite3 (Offline DB)
OpenGL headers and libraries, SDL, SDL_image (Benchmarks)

Please note that, even if using GtkBuilder, at the moment libglade is still needed 
for correct localization of the GUI.

OPTIONAL:
MatPlotLib, NumPy (Statistical charts)
Epydoc (Docs generation)

Compiling
---------
To compile benchmarks, type:
	$ scons

Using
-----
To run the program without installing it, type:
	$ ./launch.sh

Installation
------------
To install the program, type:
	$ scons install [ root=path prefix=path ]
    $ python patchdir.py [ prefix=path ]
    $ python setup.py install [ --root=path --prefix=path ]

You can specify a prefix (e.g. /usr) and an alternative root (e.g. /mnt/hdb1).

Generating Docs
---------------
Go in the src directory then type:
	$ epydoc -o epydoc --name=globs --url=http://globs.sourceforge.net --graph=all Globs/*.py
